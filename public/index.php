<?php

defined('APP_PATH') || define('APP_PATH', realpath('..'));

require_once APP_PATH . "/app/library/Bootstrap.php";
require_once APP_PATH . "/app/library/Services.php";

$serviceContainer = new Httpcb\Services();

/**
 * Bootstrap the application.
 */
$bootstrap = new \Httpcb\Bootstrap($serviceContainer);

$bootstrap
	->prepare()
	->run($_SERVER["REQUEST_URI"]);
