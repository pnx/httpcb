
# ACL

The ACL is defined as follows:

## Roles

Roles define a group of user. like Author, Admin, Guest etc.
Each role can inherit other roles with the "inherit" key.
Each role can gain access to a zone (explained later) by the
"allowed-zones" key. Per default a role is denied access to all zones.

## Resources

Resources maps directly to `controller` names. If a controller is not
under the default module. `<module>/<controller>` format is used instead.

A special wildcard `*` character can be used to allow access to all
controllers (most likely only useful for non-default modules). 

For example the resource `backend/*` Matches all controllers under 
the backend module.

### Special controllers.

There a 2 controllers that are a bit special,
`index` and `error` resources are always accessible by everyone (e.g. they
are not part of the ACL).

## Access levels.

These are not used in this system. a hardcoded "All" level is used.

## Zones

Zones defines as 1 or more resources. for example an "backend" zone can
have 2 controllers/resources (*site-config*, *user-manager*)

Zones can also defines entire modules


# Example config.

acl.yml
```yaml
acl:
 roles:
  guest:	# Guests are only allowed to access the public zone.
    allowed-zones: public
    description: Non logged in users
  user:		# Users inherits the guest role + has access to user zone.
    inherits: guest
    allowed-zones: user
    description: Logged in users
  admin:	# Admins inherits the user role + has access to backend zone.
    inherits: user
    description: Administrators
    allowed-zones: backend

 zones:
  # Public zone is the start page in 
  # index controller + login/logout in auth.
  public:   [ auth ]
  # User zone can access profile and settings controllers
  user:     [ profile, settings ]
  # Backend zone is the entire backend module.
  backend:  backend/*
```
