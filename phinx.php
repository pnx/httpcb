<?php

require_once 'vendor/autoload.php';

use Phalcon\Config\Adapter\Yaml as Config;

$config = new Config('app/config/local.yml');

$phinx = [
    'paths' => [
        'migrations' => 'app/migrations'
    ],
    'environments' => [
        'default_database' => 'main',
    ]
];


$db = $config->get('database');
if ($db) {
    $phinx['environments']['main'] = [
        'adapter'   => strtolower($db->adapter),
        'host'      => $db->host,
        'user'      => $db->username,
        'pass'      => $db->password,
        'name'      => $db->dbname,
        'charset'   => $db->charset,
    ];
}

return $phinx;
