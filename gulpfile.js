var { src, dest, watch, series }  = require('gulp');
var rename      = require('gulp-rename');
var sass 		= require('gulp-sass')(require('sass'));
var cssminify   = require('gulp-csso');
var concat      = require('gulp-concat');
var uglify      = require('gulp-uglify');

// ----------------------------------
//   Config
// ----------------------------------

var config = {
    sass: {
        pattern: 'app/assets/sass/**/*.scss',
        src: 'app/assets/sass/application.scss',
        opt: {
            includePaths: [ 'node_modules' ]
        },
        outputDir: 'public/css',
        outputName: 'application'
    },
    js: {
        src: [
			'node_modules/@popperjs/core/dist/umd/popper.js',
            // Bootstrap
			'node_modules/bootstrap/dist/js/bootstrap.js',
            // Font Awesome
            'app/assets/js/font-awesome/regular.js',
            'app/assets/js/font-awesome/solid.js',
            'app/assets/js/font-awesome/brands.js',
            'app/assets/js/font-awesome/fontawesome.js'
        ],
        outputDir: 'public/js'
    }
};

// ----------------------------------
//   Tasks
// ----------------------------------

function build_css() {
	return src(config.sass.src)
        .pipe(sass(config.sass.opt))
        .pipe(cssminify())
        .pipe(rename({ basename: config.sass.outputName, suffix: '.min'}))
        .pipe(dest(config.sass.outputDir))
}

function build_js() {
	return src(config.js.src)
        .pipe(uglify())
        .pipe(concat('application.min.js'))
        .pipe(dest(config.js.outputDir));
}

function watch_sass() {
    return watch(config.sass.pattern, {"usePolling":true}, build_css);
}

exports.build_css = build_css;
exports.build_js = build_js;
exports.build = series(build_css, build_js);
exports.watch_sass = watch_sass;
exports.watch = series(watch_sass);
exports.default = watch;
