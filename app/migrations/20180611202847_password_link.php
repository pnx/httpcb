<?php


use Phinx\Migration\AbstractMigration;

class PasswordLink extends AbstractMigration
{
    public function up()
    {
        $this->table('password_link')
            ->addColumn('public_id', 'string', ['length' => 12])
            ->addColumn('user_id', 'integer')
            ->addForeignKey('user_id', 'user', ['id'], ['constraint' => 'FK_password_link_user'])
            ->addColumn('date', 'datetime', ['default' => 'CURRENT_TIMESTAMP'])
            ->addColumn('password', 'string', ['limit' => 255, 'null' => true])
            ->save();
    }
}
