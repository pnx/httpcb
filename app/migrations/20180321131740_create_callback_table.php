<?php


use Phinx\Migration\AbstractMigration;

class CreateCallbackTable extends AbstractMigration
{
    public function up()
    {
        $table = $this->table('callback');
        $table->addColumn('public_id', 'string', [
            'length' => 12,
            'null' => false,
        ])->addIndex('public_id', ['name' => 'UNIQUE_public_id', 'unique' => true]);

        $table->addColumn('userid', 'integer', [
            'null' => true,
        ])->addForeignKey('userid', 'user', ['id'], ['constraint' => 'FK_user']);

        $table->addColumn('name', 'string', [
            'length' => 64,
            'null' => false,
        ]);

        $table->addColumn('created_at', 'timestamp', [
            'default' => 'CURRENT_TIMESTAMP'
        ]);

        $table->addColumn('last_request', 'timestamp', [
            'default' => null,
            'null' => true
        ]);

        $table->save();
    }
}
