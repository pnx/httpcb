<?php


use Phinx\Migration\AbstractMigration;

class UserRemoveGithubUser extends AbstractMigration
{
    public function up()
    {
        $this->table('user')
            ->removeColumn('github_user')
            ->save();
    }
}
