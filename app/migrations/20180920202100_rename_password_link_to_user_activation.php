<?php


use Phinx\Migration\AbstractMigration;

class RenamePasswordLinkToUserActivation extends AbstractMigration
{
    public function up()
    {
        $this->table('password_link')
            ->rename('user_activation')
            ->changeColumn('public_id', 'string', [
                'limit' => 40
            ])
            ->addColumn('used', 'integer', [
                'limit' => 1,
                'default' => 0,
                'after' => 'user_id'
            ])->save();

        $this->table('user_activation')
            ->renameColumn('public_id', 'activation_key')
            ->save();

        // Set used = 1 on all rows where password = null
        $this->getQueryBuilder()
            ->update('user_activation')
            ->set('used', 1)
            ->where('password IS NULL OR LENGTH(password) < 1')
            ->execute();
    }
}
