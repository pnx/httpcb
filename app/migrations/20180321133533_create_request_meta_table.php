<?php


use Phinx\Migration\AbstractMigration;

class CreateRequestMetaTable extends AbstractMigration
{
    public function up()
    {
        $table = $this->table('request_meta');

        $table->addColumn('callbackid', 'integer');
        $table->addForeignKey(
            'callbackid',
            'callback',
            ['id'],
            ['constraint' => 'FK_callback']
        );

        $table->addColumn('source_ip', 'string', [
            'limit' => 50,
            'null' => true,
        ]);

        $table->addColumn('method', 'enum', [
            'null' => false,
            'default' => 'GET',
            'values' => ['GET', 'POST']
        ]);

        $table->addColumn('uri', 'string', [
            'limit' => 255,
            'null' => true,
        ]);

        $table->addColumn('timestamp', 'timestamp', [
            'default' => 'CURRENT_TIMESTAMP',
            'null' => false,
        ]);

        $table->save();
    }
}
