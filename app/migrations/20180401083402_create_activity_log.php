<?php


use Phinx\Migration\AbstractMigration;

class CreateActivityLog extends AbstractMigration
{
    public function up()
    {
        $table = $this->table('activity_log');

        $table->addColumn('timestamp', 'datetime', [
            'default' => 'CURRENT_TIMESTAMP',
            'after' => 'email'
        ]);

        $table->addColumn('user_id', 'integer')
            ->addForeignKey('user_id', 'user', ['id']);

        $table->addColumn('ip', 'string', [
            'null' => true,
            'length' => 50,
        ]);

        $table->addColumn('message', 'string', [
            'null' => true,
            'length' => 255,
        ]);

        $table->save();
    }
}
