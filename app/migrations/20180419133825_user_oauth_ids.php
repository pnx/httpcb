<?php


use Phinx\Migration\AbstractMigration;

class UserOauthIds extends AbstractMigration
{
    public function up()
    {
        $this->table('user')
            ->addColumn('gitlab_id', 'integer', [
                'limit' => 14,
                'null' => true,
                'after' => 'github_user'
            ])
            ->addColumn('google_id', 'integer', [
                'limit' => 21,
                'null' => true,
                'after' => 'gitlab_id'
            ])->save();
    }
}
