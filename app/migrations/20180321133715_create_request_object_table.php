<?php


use Phinx\Migration\AbstractMigration;

class CreateRequestObjectTable extends AbstractMigration
{
    public function up()
    {
        $table = $this->table('request_object');

        $table->addForeignKey(
            'id',
            'request_meta',
            ['id'],
            ['constraint' => 'FK_request_meta']
        );

        $table->addColumn('headers', 'blob', [
            'null' => true,
        ]);

        $table->addColumn('body', 'blob', [
            'null' => true,
        ]);

        $table->save();
    }
}
