<?php

use Phinx\Migration\AbstractMigration;

class CreateUserTable extends AbstractMigration
{
    public function up()
    {
        $table = $this->table('user');

        $table->addColumn('username', 'string', [
            'limit' => 50,
            'null' => false
        ]);

        $table->addColumn('email', 'string', [
            'limit' => 255,
            'null' => false
        ]);

        $table->addColumn('status', 'enum', [
            'null' => false,
            'default' => 'Active',
            'values' => ['Active', 'Deleted', 'Suspended']
        ]);

        $table->addColumn('password', 'string', [
            'limit' => 255,
            'null' => true,
        ]);

        $table->save();
    }
}
