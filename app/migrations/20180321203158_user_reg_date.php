<?php

use Phinx\Migration\AbstractMigration;

class UserRegDate extends AbstractMigration
{
    /**
     * Add regdate column
     */
    public function up()
    {
        $this->table('user')
            ->addColumn('regdate', 'datetime', [
                'default' => 'CURRENT_TIMESTAMP',
                'after' => 'email'
            ])->save();
    }
}
