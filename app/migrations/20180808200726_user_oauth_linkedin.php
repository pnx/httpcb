<?php


use Phinx\Migration\AbstractMigration;

class UserOauthLinkedin extends AbstractMigration
{
    public function up()
    {
        $this->table('user')
            ->addColumn('linkedin_id', 'string', [
                'limit' => 16,
                'null' => true,
                'after' => 'google_id'
            ])->save();
    }
}
