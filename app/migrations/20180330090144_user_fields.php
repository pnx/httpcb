<?php


use Phinx\Migration\AbstractMigration;

class UserFields extends AbstractMigration
{
    /**
     * Add regdate column
     */
    public function up()
    {
        $this->table('user')
            ->addColumn('name', 'string', [
                'limit' => 128,
                'null' => true,
                'after' => 'username'
            ])
            ->addColumn('github_id', 'integer', [
                'limit' => 14,
                'null' => true,
                'after' => 'password'
            ])
            ->addColumn('github_user', 'string', [
                'limit' => 80,
                'null' => true,
                'after' => 'github_id'
            ])->save();
    }
}
