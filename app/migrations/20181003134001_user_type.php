<?php


use Phinx\Migration\AbstractMigration;

class UserType extends AbstractMigration
{
    public function up()
    {
        $this->table('user')
            ->addColumn('type', 'enum', [
                'null' => false,
                'default' => 'user',
                'values' => ['user', 'admin'],
                'after' => 'regdate'
            ])
            ->save();
    }
}
