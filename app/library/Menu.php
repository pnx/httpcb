<?php

namespace Httpcb;

use Phalcon\Tag;

use Httpcb\Acl,
    Httpcb\Navigation,
    Httpcb\Navigation\Node;

class Menu extends Tag
{
    /**
     * ACL Role
     *
     * @var string
     */
    protected $_role = null;

    /**
     * @var Navigation
     */
    protected $_navigation;

    /**
     * css class to use for the whole menu.
     *
     * @var string
     */
    protected $_menuClass = 'menu';

    /**
     * Class to use for active nodes.
     *
     * @var string
     */
    protected $_activeClass = 'active';

    /**
     * @param Navigation $navigation
     */
    public function __construct(Navigation $navigation)
    {
        $this->_navigation = $navigation;
    }

    /**
     * @return Navigation
     */
    public function getNavigation()
    {
        return $this->_navigation;
    }

    /**
     * @param Navigation $navigation
     * @return Menu
     */
    public function setNavigation(Navigation $navigation)
    {
        $this->_navigation = $navigation;
        return $this;
    }

    public function setMenuClass($class)
    {
        $this->_menuClass = (string) $class;
        return $this;
    }

    /**
     * @param $role
     */
    public function setAclRole($role)
    {
        $this->_role = $role;
    }

    /**
     * Render the menu.
     *
     * @return string
     */
    public function render($max_depth = null)
    {
        return $this->_renderMenu($this->_navigation->getChildren(), 0, $max_depth);
    }

    protected function _renderMenu($nodes, $depth, $max_depth = null)
    {
        $xhtml = '';

        foreach ($nodes as $node) {
            $xhtml .= $this->_renderNode($node, $depth, $max_depth);
        }

        if (strlen($xhtml) > 0) {

            $attribs = array();
            if (strlen($this->_menuClass) > 0) {
                $attribs['class'] = $this->_menuClass;
            }

            return self::tagHtml('ul', $attribs, false, false, true)
                . $xhtml
                . self::tagHtmlClose('ul', true);
        }
        return $xhtml;
    }

    protected function _renderNode(Node $node, $depth, $max_depth = null)
    {
        $xhtml = '';

        // ACL.
        $resource = $node->getResource();
        if (strlen($this->_role) > 0 && strlen($resource) > 0 && $this->getDI()->has('acl')) {
            $acl = $this->getDI()->get('acl');
            if (!$acl->isAllowed($this->_role, $resource, 'Read')) {
                return $xhtml;
            }
        }

        // Only render this node if it is visible and has a caption.
        if (!$node->isVisible() || strlen($node->getCaption()) < 1) {
            return $xhtml;
        }

        $xhtml = self::tagHtml(
            'li',
            $node->isActive()
                ? array('class' => $this->_activeClass) : null,
            false,
            false,
            true
        );

        // Generate the link.
        $xhtml .= self::linkTo($node->getHref(), $node->getCaption());

        if (
            $node->isActive() && $node->hasChildren()
            && ($max_depth === null || $depth < $max_depth)
        ) {

            $xhtml .= $this->_renderMenu($node->getChildren(), $depth + 1, $max_depth);
        }

        return $xhtml . self::tagHtmlClose('li', true);
    }
}
