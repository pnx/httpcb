<?php

namespace Httpcb\OAuth\Adapter;

interface AdapterInterface
{
    /**
     * Construct a adapter object.
     *
     * @param string $provider_name
     * @param array $options
     */
    public function __construct($provider_name, $options);

    /**
     * Get the provider name
     *
     * @return string
     */
    public function getProviderName();

    /**
     * Get the authorization url.
     *
     * @return string
     */
    public function getAuthorizationUrl();

    /**
     * Fetch the AccessToken using the temporary code returned from provider.
     *
     * @param $code
     * @return
     */
    public function fetchAccessToken($code);

    /**
     * Get resource data from provider.
     *
     * @return array
     */
    public function getResourceData();
}
