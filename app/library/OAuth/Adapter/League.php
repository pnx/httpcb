<?php

namespace Httpcb\OAuth\Adapter;

use League\OAuth2\Client\Provider\AbstractProvider,
    League\OAuth2\Client\Token\AccessToken;

class League implements AdapterInterface
{
    /**
     * List of all supported providers and their class name.
     *
     * @var array
     */
    protected $_providerClasses = array(
        'github'    => '\League\OAuth2\Client\Provider\Github',
        'gitlab'    => '\Omines\OAuth2\Client\Provider\Gitlab',
        'google'    => '\League\OAuth2\Client\Provider\Google',
        'linkedin'  => '\League\OAuth2\Client\Provider\LinkedIn',
    );

    /**
     * @var AbstractProvider
     */
    protected $_provider;

    /**
     * @var AccessToken
     */
    protected $_accessToken;

    protected $_options;

    /**
     * {@inheritDoc}
     * @throws Exception
     */
    public function __construct($provider_name, $options)
    {
        if (!array_key_exists($provider_name, $this->_providerClasses)) {
            throw new Exception("Provider '{$provider_name}' is not supported.");
        }

        $className = $this->_providerClasses[$provider_name];
        $provider = new $className($options);

        if (!($provider instanceof AbstractProvider)) {
            // TODO: Throw a better exception class :)
            throw new Exception("Provider object must be an instance of League\\OAuth2\\Client\\Provider\\AbstractProvider");
        }

        $this->_provider = $provider;
        $this->_options = $options;
    }

    /**
     * {@inheritDoc}
     */
    public function getProviderName()
    {
        try {
            $reflection = new \ReflectionClass($this->_provider);
            return $reflection->getShortName();
        } catch (\ReflectionException $ex) {
            return '';
        }
    }

    /**
     * {@inheritDoc}
     */
    public function getAuthorizationUrl()
    {
        return $this->_provider->getAuthorizationUrl($this->_options);
    }

    /**
     * {@inheritDoc}
     */
    public function fetchAccessToken($code)
    {
        $this->_accessToken = $this->_provider->getAccessToken('authorization_code', [
            'code' => $code
        ]);
    }

    /**
     * {@inheritDoc}
     */
    public function getResourceData()
    {
        $resource = $this->_provider->getResourceOwner($this->_accessToken);
        return $resource->toArray();
    }
}
