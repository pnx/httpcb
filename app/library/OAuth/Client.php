<?php

namespace Httpcb\OAuth;

use Httpcb\OAuth\Adapter\AdapterInterface;

class Client
{
    /**
     * Adapter to use.
     *
     * @var AdapterInterface
     */
    protected $_adapter;

    /**
     * Access Token
     *
     * @var string
     */
    protected $_token;

    public function __construct(AdapterInterface $adapter)
    {
        $this->_adapter = $adapter;
    }

    public function getAuthorizationUrl()
    {
        return $this->_adapter->getAuthorizationUrl();
    }

    /**
     * @param string $code
     * @return UserDataInterface
     */
    public function authenticate($code)
    {
        $this->_adapter->fetchAccessToken($code);

        return $this->getUserData();
    }

    /**
     * @return UserDataInterface
     */
    public function getUserData()
    {
        $data = $this->_adapter->getResourceData();
        return $this->_createUserData($data);
    }

    protected function _createUserData(array $data)
    {
        $name = $this->_adapter->getProviderName();
        $class = "Httpcb\\OAuth\\UserData\\{$name}";
        return new $class($data);
    }
}
