<?php

namespace Httpcb\OAuth\UserData;

class Google extends UserData
{
    protected $_provider = 'Google';

    /**
     * {@inheritDoc}
     */
    public function __construct(array $data)
    {
        $this->_id      = $data['id'];
        $this->_name    = $data['displayName'];

        if (isset($data['emails'][0]['value'])) {
            $this->_email = $data['emails'][0]['value'];
        }
    }
}
