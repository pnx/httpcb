<?php

namespace Httpcb\OAuth\UserData;

interface UserDataInterface
{
    public function __construct(array $data);

    /**
     * The providers name.
     *
     * @return string
     */
    public function getProvider();

    /**
     * Owners ID (userid)
     *
     * @return string
     */
    public function getId();

    /**
     * the username
     *
     * @return string
     */
    public function getUsername();

    /**
     * Full name.
     *
     * @return string
     */
    public function getName();

    /**
     * First name
     *
     * @return string
     */
    public function getFirstname();

    /**
     * Last name
     *
     * @return string
     */
    public function getLastname();

    /**
     * Email address
     *
     * @return string
     */
    public function getEmail();

    /**
     * @return array
     */
    public function toArray();
}
