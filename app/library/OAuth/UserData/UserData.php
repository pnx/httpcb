<?php

namespace Httpcb\OAuth\UserData;

abstract class UserData implements UserDataInterface
{
    protected $_provider = null;

    protected $_id = null;

    protected $_username = null;

    protected $_email = null;

    protected $_name = null;

    protected $_firstname = null;

    protected $_lastname = null;

    /**
     * {@inheritDoc}
     */
    public function getProvider()
    {
        return $this->_provider;
    }

    /**
     * {@inheritDoc}
     */
    public function getId()
    {
        return $this->_id;
    }

    /**
     * {@inheritDoc}
     */
    public function getUsername()
    {
        return $this->_username;
    }

    /**
     * {@inheritDoc}
     */
    public function getName()
    {
        if ($this->_name === null && strlen($this->_firstname) > 0) {
            $name = $this->_firstname;
            if (strlen($this->_lastname) > 0) {
                $name .= ' ' . $this->_lastname;
            }
            return $name;
        }
        return $this->_name;
    }

    /**
     * {@inheritDoc}
     */
    public function getFirstname()
    {
        if ($this->_firstname === null) {
            $pos = strpos($this->getName(), ' ');
            return $pos !== false ? substr($this->getName(), 0, $pos) : $this->getName();

        }
        return $this->_firstname;
    }

    /**
     * {@inheritDoc}
     */
    public function getLastname()
    {
        if ($this->_lastname === null) {
            $pos = strpos($this->getName(), ' ');
            return $pos !== false ? substr($this->getName(), $pos+1) : null;
        }
        return $this->_lastname;
    }

    /**
     * {@inheritDoc}
     */
    public function getEmail()
    {
        return $this->_email;
    }

    public function toArray()
    {
        $data = [];
        foreach(get_class_methods($this) as $method) {
            if (substr($method, 0, 3) == 'get') {
                $field = lcfirst(substr($method, 3));
                $data[$field] = $this->$method();
            }
        }
        return $data;
    }
}
