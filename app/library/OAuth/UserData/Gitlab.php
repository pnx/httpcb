<?php

namespace Httpcb\OAuth\UserData;

class Gitlab extends UserData
{
    protected $_provider = 'Gitlab';

    /**
     * {@inheritDoc}
     */
    public function __construct(array $data)
    {
        $this->_id = $data['id'];
        $this->_username = $data['username'];
        $this->_name = $data['name'];
        $this->_email = $data['email'];
    }
}
