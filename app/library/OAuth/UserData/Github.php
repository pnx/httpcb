<?php

namespace Httpcb\OAuth\UserData;

class Github extends UserData
{
    protected $_provider = 'Github';

    /**
     * {@inheritDoc}
     */
    public function __construct(array $data)
    {
        $this->_id = $data['id'];
        $this->_username = $data['login'];
        $this->_name = $data['name'];
        $this->_email = $data['email'];
    }
}
