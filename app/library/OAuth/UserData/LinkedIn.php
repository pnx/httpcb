<?php

namespace Httpcb\OAuth\UserData;

class LinkedIn extends UserData
{
    protected $_provider = 'LinkedIn';

    /**
     * {@inheritDoc}
     */
    public function __construct(array $data)
    {
        $this->_id = $data['id'];
        $this->_firstname = isset($data['firstName']) ? $data['firstName'] : null;
        $this->_lastname = isset($data['lastName']) ? $data['lastName'] : null;
        $this->_email = $data['emailAddress'];
    }
}
