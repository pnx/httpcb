<?php

namespace Httpcb\Auth;

/**
 * Class Result
 * @package Httpcb\Auth
 */
class Result
{
    /**
     * Codes
     */
    const SUCCESS                       =  1;
    const FAILURE_INVALID_CREDENTIALS   = -1;
    const FAILURE_IDENTITY_NOT_FOUND    = -2;
    const FAILURE_ACCOUNT_SUSPENDED     = -3;

    /**
     * @var int
     */
    protected $_code;

    /**
     * Constructor
     *
     * @param $code
     */
    public function __construct($code)
    {
        $this->_code = (int) $code;
    }

    /**
     * Get the result code for this authentication attempt.
     *
     * @return int
     */
    public function getCode()
    {
        return $this->_code;
    }

    /**
     * Returns true if the authentication was sucessfull, false otherwise.
     *
     * @return bool
     */
    public function isValid()
    {
        return $this->_code == self::SUCCESS;
    }
}
