<?php

namespace Httpcb\ViewHelper;

use Phalcon\Di\DiInterface;
use Phalcon\Di\InjectionAwareInterface;

abstract class AbstractHelper implements InjectionAwareInterface
{
    protected $_di;

    /**
     * Sets the dependency injector
     *
     * @param DiInterface $container
     */
    public function setDI(DiInterface $container): void
    {
        $this->_di = $container;
    }

    /**
     * Returns the internal dependency injector
     *
     * @return DiInterface
     */
    public function getDI(): DiInterface
    {
        return $this->_di;
    }
}
