<?php

namespace Httpcb\ViewHelper;

/**
 * Class UrlStyle
 *
 * Tags diffrent parts in a url around <span> tags with class names.
 *
 * classes/parts are:
 *  scheme
 *  host
 *  port
 *  path
 *  query
 *  fragment
 *
 * @package ViewHelper
 */
class UrlStyle extends AbstractHelper
{
    /**
     * Prefix (or namespace) for css classes.
     *
     * @var string
     */
    protected $_classPrefix = 'url-';

    public function urlStyle($url)
    {
        $parts = parse_url($url);

        if ($parts === null) {
            return $url;
        }

        $xhtml = '';

        if (isset($parts['scheme'])) {
            $xhtml .= $this->_htmlElement('scheme', $parts['scheme'] . '://');
        }

        if (isset($parts['host'])) {
            $xhtml .= $this->_htmlElement('host', $parts['host']);
        }

        if (isset($parts['port'])) {
            $xhtml .= $this->_htmlElement('port', ':' . $parts['port']);
        }

        $xhtml .= $this->_htmlElement('path', $parts['path']);

        if (isset($parts['query'])) {
            $xhtml .= $this->_htmlElement('query', '?' . $parts['query']);
        }

        if (isset($parts['fragment'])) {
            $xhtml .= $this->_htmlElement('fragment', '#' . $parts['fragment']);
        }

        return $xhtml;
    }

    protected function _htmlElement($class, $content)
    {
        $class = $this->_classPrefix . $class;
        return '<span class="' . $class . '">' . $content . '</span>';
    }
}
