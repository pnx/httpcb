<?php

namespace Httpcb\ViewHelper;

/**
 * Class ServerUrl
 *
 * @package ViewHelper
 */
class ServerUrl extends AbstractHelper
{
    protected $_request;

    public function getScheme()
    {
        return $this->_getRequest()->getScheme();
    }

    public function getHost()
    {
        return $this->_getRequest()->getHttpHost();
    }

    public function getPort()
    {
        return $this->_getRequest()->getPort();
    }

    public function serverUrl()
    {
        $port = $this->getPort();
        $scheme = $this->getScheme();

        // remove port if it's the default port.
        if (($scheme == 'http' && $port == 80)
            ||  ($scheme == 'https' && $port == 443)
        ) {
            $port = null;
        }

        $url = $scheme . '://' . $this->getHost();
        if ($port !== null) {
            $url .= ':' . $port;
        }
        return $url;
    }

    /**
     * @return \Phalcon\Http\RequestInterface
     */
    protected function _getRequest()
    {
        if ($this->_request === null) {
            $this->_request = $this->getDI()->getRequest();
        }
        return $this->_request;
    }
}
