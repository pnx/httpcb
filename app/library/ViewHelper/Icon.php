<?php

namespace Httpcb\ViewHelper;

/**
 * Class Icon
 *
 * @package ViewHelper
 */
class Icon extends AbstractHelper
{
    protected $_prefix = [
        'brand'     => 'fab',
        'regular'   => 'far',
        'solid'     => 'fas'
    ];

    public function icon($name, $args = array())
    {
        list($prefix, $name) = $this->_parseName($name);

        $classes = array(
            'icon',
            $prefix,
            'fa-' . $name
        );

        if (is_array($args)) {

            foreach ($args as $arg) {
                $classes[] .= 'fa-' . $arg;
            }
        }

        $classes = implode(' ', $classes);
        return '<i class="' . $classes . '"></i>';
    }

    protected function _parseName($name)
    {
        $parts = explode('/', $name);
        if (count($parts) > 1) {
            $prefix = $parts[0];
            $name = $parts[1];
        } else {
            $prefix = 'regular';
        }
        return array($this->_prefix[$prefix], $name);
    }
}
