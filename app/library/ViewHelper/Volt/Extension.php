<?php

namespace Httpcb\ViewHelper\Volt;

use Phalcon\Di\DiInterface,
    Phalcon\Di\InjectionAwareInterface;

class Extension implements InjectionAwareInterface
{
    protected $_serviceKey = 'HttpcbViewHelper';

    /**
     * @var DiInterface
     */
    protected $_di;

    public function __construct(DiInterface $dependencyInjector)
    {
        $this->_di = $dependencyInjector;

        if (!$this->_di->has($this->_serviceKey)) {
            $this->_di->set($this->_serviceKey, 'Httpcb\ViewHelper\Service', true);
        }
    }

    public function compileFunction($name, $args)
    {
        // Get the view helper service.
        $service = $this->_di->getShared($this->_serviceKey);

        // Search for the helper in service.
        if ($service->has($name)) {
            return "\$this->{$this->_serviceKey}->{$name}({$args})";
        }
        return false;
    }

    /**
     * Sets the dependency injector
     *
     * @param DiInterface $container
     */
    public function setDI(DiInterface $container) : void
    {
        $this->_di = $container;
    }

    /**
     * Returns the internal dependency injector
     *
     * @return DiInterface
     */
    public function getDI() : DiInterface
    {
        return $this->_di;
    }
}
