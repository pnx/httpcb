<?php

namespace Httpcb\ViewHelper;

use Phalcon\Di\DiInterface,
    Phalcon\Di\InjectionAwareInterface;

class Service implements InjectionAwareInterface
{
    protected $_helpers = array();

    protected $_di;

    /**
     * Sets the dependency injector
     *
     * @param DiInterface $container
     */
    public function setDI(DiInterface $container): void
    {
        $this->_di = $container;
    }

    /**
     * Returns the internal dependency injector
     *
     * @return DiInterface
     */
    public function getDI(): DiInterface
    {
        return $this->_di;
    }

    public function set($name, AbstractHelper $helper)
    {
        $helper->setDI($this->getDI());

        $this->_helpers[$name] = $helper;
    }

    public function has($name)
    {
        return $this->_locateHelper($name) !== false;
    }

    public function get($name)
    {
        if ($this->has($name)) {
            return $this->_helpers[$name];
        }
        return false;
    }

    public function __call($name, $args)
    {
        $helper = $this->get($name);
        if ($helper) {
            return call_user_func_array(array($helper, $name), $args);
        }
        return false;
    }

    protected function _locateHelper($name)
    {
        if (array_key_exists($name, $this->_helpers)) {
            return $this->_helpers[$name];
        }

        $class = 'Httpcb\\ViewHelper\\' . ucfirst($name);
        if (class_exists($class)) {
            $helper = new $class();
            $this->set($name, $helper);
            return $helper;
        }
        return false;
    }
}
