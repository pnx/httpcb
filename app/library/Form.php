<?php

namespace Httpcb;

use Phalcon\Forms\Form as FormBase,
    Phalcon\Forms\Element\AbstractElement;

class Form extends FormBase
{
    public function renderDecorated($name, $opt = [])
    {
        $options = [
            'label-class' => 'col-form-label text-end',
            'class' => ['col-sm-10'],
            'message' => ''
        ];

        $ele = $this->get($name);

        if (isset($opt['label-length'])) {
            $length = (int) $opt['label-length'];
        } else {
            $length = 2;
        }
        $options['label-class'] .= ' col-sm-' . $length;

        if (isset($opt['length'])) {

            $len = $opt['length'];

            if ($len === 'full') {
                $options['class'] = [];
            } else {
                $options['class'] = ['col-sm-' . $len];
            }

            unset($opt['length']);
        }

        return $this->_render($ele, $options);
    }

    protected function _render(AbstractElement $ele, $opt)
    {
        $classes = ['class' => 'form-control'];
        if ($ele->hasMessages()) {
            $classes['class'] .= ' is-invalid';
        }

        $xhtml = '';

        if (strlen($ele->getLabel()) > 0) {

            $xhtml .= sprintf(
                '<label class="%s" for="%s">%s</label>',
                $opt['label-class'],
                $ele->getName(),
                $ele->getLabel()
            );
        }

        $xhtml .= '<div class="' . implode(' ', $opt['class']) . '">'
            . $ele->render($classes);

        if ($ele->hasMessages()) {
            $msg = $ele->getMessages()->current();
            $xhtml .= '<span class="invalid-feedback">' . $msg . '</span>';
        }

        $xhtml .= '</div>';

        return $xhtml;
    }
}
