<?php

namespace Httpcb;

use App\Model\Data\User,
    Httpcb\OAuth\UserData\UserDataInterface,
    Httpcb\Auth\Result,
    Phalcon\Di\Injectable;

class Auth extends Injectable
{
    const SESSION_KEY = 'auth';
    const IMPERSONATOR_ID = 'auth.impersonator';

    /**
     * Login using email/user + password combination.
     *
     * @param string $email_username
     * @param string $password
     * @return Result
     */
    public function login($email_username, $password)
    {
        // Look for a user with this username/email.
        $user = User::findFirstByUsernameOrEmail($email_username);

        if ($user) {

            if ($user->status == User::STATUS_SUSPENDED) {
                return new Result(Result::FAILURE_ACCOUNT_SUSPENDED);
            }

            // Verify password
            $hash = $user->getPassword();
            if (strlen($hash) > 1 && $this->security->checkHash($password, $hash)) {

                $this->setIdentity($user->getId());

                $this->eventsManager->fire('auth:onLogin', $this, 'password');

                return new Result(Result::SUCCESS);
            }
            return new Result(Result::FAILURE_INVALID_CREDENTIALS);
        }
        return new Result(Result::FAILURE_IDENTITY_NOT_FOUND);
    }

    /**
     * Login using OAuth
     *
     * @param UserDataInterface $data
     * @return Result
     */
    public function loginOauth(UserDataInterface $data)
    {
        $user = User::findFirstByOAuthID($data);

        // Did not find any user.
        if ($user) {

            if ($user->getStatus() == User::STATUS_SUSPENDED) {
                return new Result(Result::FAILURE_ACCOUNT_SUSPENDED);
            }

            $this->setIdentity($user->getId());

            $this->eventsManager->fire(
                'auth:onLogin',
                $this,
                "OAuth {$data->getProvider()}"
            );


            return new Result(Result::SUCCESS);
        }
        return new Result(Result::FAILURE_IDENTITY_NOT_FOUND);
    }

    /**
     * The system logs in a user (without credentials).
     *
     * @param User $user
     */
    public function systemLogin(User $user)
    {
        $this->setIdentity($user->getId());
        $this->eventsManager->fire('auth:onLogin', $this, 'System');
    }

    public function getImpersonator()
    {
        $id = $this->session->get(self::IMPERSONATOR_ID);
        return $id !== null ? User::findFirst($id) : null;
    }

    /**
     * Impersonate a user
     *
     * @param User $user
     */
    public function impersonate(User $user)
    {
        $current = $this->getIdentity();
        if ($current === null) {
            throw new \InvalidArgumentException("Need to be authenticated to be able to impersonate someone");
        }

        if ($current->getId() === $user->getId()) {
            // Same user
            throw new \DomainException("Can't impersonate yourself");
        }

        $this->session->set(self::IMPERSONATOR_ID, $current->getId());
        $this->setIdentity($user->getId());
        $this->eventsManager->fire('auth:onImpersonate', $this, $current);
    }

    public function impersonateClear($imp_id)
    {
        $this->session->remove(self::IMPERSONATOR_ID);
        $this->session->set(self::SESSION_KEY, $imp_id);
    }

    /**
     * @param $identity
     * @return Auth
     */
    public function setIdentity($identity)
    {
        $this->session->set(self::SESSION_KEY, $identity);
        return $this;
    }

    /**
     * return \Model\Data\User
     */
    public function getIdentity()
    {
        $id = $this->session->get(self::SESSION_KEY);

        if ($id !== null) {
            return User::findFirst($id);
        }
        return null;
    }

    /**
     * return \Model\Data\User
     */
    public function getUser()
    {
        if ($this->hasIdentity()) {

            $id = $this->session->get(self::SESSION_KEY);

            return User::findFirst($id);
        }
        return null;
    }

    public function hasIdentity()
    {
        return $this->getIdentity() !== NULL;
    }

    /**
     * Clears the identity information.
     *
     * @return Auth
     */
    public function clearIdentity()
    {
        $imp_id = $this->session->get(self::IMPERSONATOR_ID);
        if ($imp_id !== null) {
            $this->impersonateClear($imp_id);
        } else {
            $this->session->remove(self::SESSION_KEY);
        }
        return $this;
    }
}
