<?php

namespace Httpcb;

class Navigation extends Navigation\Container
{
    /**
     * Navigation constructor.
     * @param $config
     * @throws Navigation\Exception
     */
    public function __construct($config)
    {
        foreach ($config as $node) {
            $this->addChild($node);
        }
    }
}
