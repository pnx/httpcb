<?php

namespace Httpcb;

use SendGrid\Mail\Mail as SendGridMail;
use SendGrid;

class Mail
{
    /**
     * @var SendGrid
     */
    protected $_sendgrid;

    public function __construct(SendGrid $sendgrid)
    {
        $this->_sendgrid = $sendgrid;
    }

    /**
     * Send an email.
     *
     * @param string $subject
     * @param string $to_address
     * @param string $body
     * @param string $content_type
     */
    public function send($subject, $to_address, $body, $content_type = 'text/html')
    {
        $mail = new SendGridMail();
        $mail->setFrom('noreply@shufflingpixels.com');
        $mail->setSubject($subject);
        $mail->addTo($to_address);
        $mail->addContent($content_type, $body);

        $this->_sendgrid->send($mail);
    }
}
