<?php

namespace Httpcb\Navigation;

class Container
{
    /**
     * Children
     *
     * @var array
     */
    protected $_children = array();

    /**
     * @return array
     */
    public function getChildren()
    {
        return $this->_children;
    }

    /**
     * @return bool
     */
    public function hasChildren()
    {
        return empty($this->getChildren()) === false;
    }

    /**
     * @param $child
     * @return Container
     * @throws Exception
     */
    public function addChild($child)
    {
        if (is_array($child)) {

            $node = new Node();

            foreach ($child as $k => $v) {

                if ($k == 'children') {
                    continue;
                }

                $node->{'set' . ucfirst($k)}($v);
            }

            if (isset($child['children'])) {

                foreach ($child['children'] as $c_data) {
                    $node->addChild($c_data);
                }
            }

            $child = $node;
        }

        if (!($child instanceof Node)) {
            throw new Exception('Must be of type node.');
        }

        $this->_children[] = $child;

        $child->setParent($this);

        return $this;
    }

    /**
     * @param $children
     * @return Container
     * @throws Exception
     */
    public function addChildren($children)
    {
        foreach ($children as $child) {
            $this->addChild($child);
        }
        return $this;
    }
}
