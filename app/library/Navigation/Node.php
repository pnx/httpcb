<?php

namespace Httpcb\Navigation;

use \Phalcon\Di;

class Node extends Container
{
    /**
     * Caption
     *
     * @var string
     */
    protected $_caption = null;

    /**
     * Controller name
     *
     * @var string
     */
    protected $_controller = null;

    /**
     * Controller action.
     *
     * @var string
     */
    protected $_action = null;

    /**
     * ACL Resource.
     *
     * @var string
     */
    protected $_resource = null;

    /**
     * Route name.
     *
     * @var string
     */
    protected $_route = null;

    /**
     * Active flag.
     *
     * @var bool
     */
    protected $_active = null;

    /**
     * Visible flag
     *
     * @var bool
     */
    protected $_visible = true;

    /**
     * Parent container. Null if no parent exists.
     *
     * @var Container|null
     */
    protected $_parent = null;

    /**
     * @param Container $parent
     * @return Node
     */
    public function setParent(Container $parent)
    {
        $this->_parent = $parent;
        return $this;
    }

    /**
     * @return Container|null
     */
    public function getParent()
    {
        return $this->_parent;
    }

    /**
     * @return string
     */
    public function getCaption()
    {
        return $this->_caption;
    }

    /**
     * @param string $caption
     * @return Node
     */
    public function setCaption($caption)
    {
        $this->_caption = (string) $caption;
        return $this;
    }

    /**
     * @return string
     */
    public function getController()
    {
        return $this->_controller;
    }

    /**
     * @param string $controller
     * @return Node
     */
    public function setController($controller)
    {
        $this->_controller = (string) $controller;
        return $this;
    }

    /**
     * @return string
     */
    public function getAction()
    {
        return $this->_action;
    }

    /**
     * @param string $action
     * @return Node
     */
    public function setAction($action)
    {
        $this->_action = (string) $action;
        return $this;
    }

    /**
     * @return string
     */
    public function getResource()
    {
        return $this->_resource;
    }

    /**
     * @param string $resource
     * @return Node
     */
    public function setResource($resource)
    {
        $this->_resource = (string) $resource;
        return $this;
    }

    /**
     * @return string
     */
    public function getRoute()
    {
        return $this->_route;
    }

    /**
     * @param string $route
     * @return Node
     */
    public function setRoute($route)
    {
        $this->_route = (string) $route;
        return $this;
    }

    /**
     * Get the href for this node.
     *
     * @return string
     */
    public function getHref()
    {
        /** @var \Phalcon\Mvc\Url */
        $url = Di::getDefault()->get('url');

        // Assemble route if set.
        if (strlen($this->getRoute()) > 0) {

            $href = [
                'for'           => $this->getRoute(),
                'controller'    => $this->getController(),
                'action'        => $this->getAction()
            ];
        }
        // Otherwise, use default route.
        else {
            $href = $this->getController();
            if (is_string($this->getAction())) {
                $href .= '/' . $this->getAction();
            }
        }

        return $url->get($href);
    }

    /**
     * @param $value
     * @return Node
     */
    public function setActive($value)
    {
        $this->_active = (bool) $value;
        return $this;
    }

    /**
     * @return bool
     */
    public function isActive()
    {
        // If active flag is not set explicitly.
        // Test this node against the current request.
        if ($this->_active === null) {

            // But only if the node is visible.
            if ($this->isVisible() === false) {
                return false;
            }

            // first. Check children.
            foreach ($this->getChildren() as $child) {

                if ($child->isActive() == true) {
                    $this->setActive(true);
                    return $this->_active;
                }
            }

            $dispatcher = Di::getDefault()->get('dispatcher');

            $controller = strtolower($dispatcher->getControllerName());
            $action = strtolower($dispatcher->getActionName());

            $active = $controller == $this->_controller && $action == $this->_action;
            $this->setActive($active);
        }

        return $this->_active;
    }

    public function setVisible($value)
    {
        $this->_visible = (bool) $value;
        return $this;
    }

    public function isVisible()
    {
        return $this->_visible;
    }
}
