<?php

namespace Httpcb;

class Debug
{

    public static function dump($var, $label = null, $echo = true)
    {
        // format the label
        $label = ($label === null) ? '' : rtrim($label) . ' ';
        // var_dump the variable into a buffer and keep the output
        ob_start();
        var_dump($var);
        $output = ob_get_clean();
        // neaten the newlines and indents
        $output = preg_replace("/\]\=\>\n(\s+)/m", "] => ", $output);
        $output = '<pre>'
            . $label
            . $output
            . '</pre>';
        if ($echo) {
            echo $output;
        }
        return $output;
    }
}
