<?php

namespace Httpcb\Validation\Validator;

use Phalcon\Validation\Message;
use Phalcon\Validation\AbstractValidator;

/**
 * The same as the default Alpha validator shipped with phalcon.
 * But this validator supports the option "allowSpace" that also
 * allow whitespaces.
 *
 * @package Validation\Validator
 */
class Alpha extends AbstractValidator
{
    /**
     * Executes the validation
     *
     * @param mixed $validation
     * @param string $field
     * @return bool
     */
    public function validate(\Phalcon\Validation $validation, $field): bool
    {
        $allowSpace = $this->getOption('allowSpace', false);

        $charlist = '[:alpha:]';
        if ($allowSpace) {
            $charlist .= '[:space:]';
        }

        $value = $validation->getValue($field);

        if (preg_match("/[^{$charlist}]/imu", $value)) {

            $label = $this->getOption('label');
            if (empty($label)) {
                $label = $validation->getLabel($field);
            }

            $message = $this->getOption('message');
            if (empty($message)) {
                $message = $validation->getDefaultMessage('Alpha');
            }

            $replace = [":field" => $label];

            $code = $this->getOption("code");
            if (is_array($code)) {
                $code = $code[$field];
            }

            $message = str_replace(array_keys($replace), $replace, $message);

            $msg = new Message($message, $field, "Alpha", $code);
            $validation->appendMessage($msg);

            return false;
        }

        return true;
    }
}
