<?php

namespace Httpcb;

use Phalcon\Di\FactoryDefault as DiDefault;
use Phalcon\Di\Injectable;
use Phalcon\Di\DiInterface;
use Phalcon\Mvc\Application;

class Bootstrap extends Injectable
{
    /**
     * @var Application
     */
    protected $_app;

    public function __construct(DiInterface $di = null)
    {
        if ($di === null) {
            $di = new DiDefault();
        }
        $this->setDI($di);

        $this->_app = new Application();
    }

    /**
     * Prepares the application to run.
     */
    public function prepare()
    {
        // Force autoloader if registered.
        // Should be needed by almost all other services.
        $di = $this->getDI();
        if ($di->has('loader')) {
            $di->get('loader');
        }

        // If debug is configured. add listener.
        if ($di->get('config')->application->debug) {
            $di->get('debugger')->listen(true, true);
        }

        // Modules
        $this->_app->registerModules([
            'main' => [ 'className' => 'App\Module\Main' ],
            'backend' => [ 'className' => 'App\Module\Backend' ],
        ]);

        $this->_app->setDefaultModule('main');

        return $this;
    }

    /**
     * Runs the application.
     */
    public function run($uri)
    {
        $this->_app->setDI($this->getDI());

		try {
	        $response = $this->_app->handle($uri);
			$response->send();
		} catch(\Exception $ex) {
			echo $ex->getMessage();
		}
    }
}
