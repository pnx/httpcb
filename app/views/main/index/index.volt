
<div class="feature-section">

    <div class="section-header">
        <h1>How Does It Work?</h1>
    </div>

    <div class="steps">
        <div class="steps-step1">
            <div class="steps-img"></div>
            <h3>1. Create a new Callback</h3>
            <p>Register a callback and you will be given a unique url</p>
        </div>

        <div class="steps-step2">
            <div class="steps-img"></div>

            <h3>2. Set endpoint URL</h3>
            <p>Configure your API that you want to test to send to that url</p>
        </div>

        <div class="steps-step3">
            <div class="steps-img"></div>
            <h3>3. Monitor traffic</h3>
            <p>Make API Calls and watch what your API will send you</p>
        </div>
    </div>

    <div class="text-center">
        <a href="/callback/new" class="button button-large button-brand">Get started</a>
    </div>
</div>
