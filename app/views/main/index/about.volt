<div class="about">

    <div class="about-main">
        <div class="section">

            <div class="section-header">
                <h1>About</h1>
            </div>

            <p>
                httpcb was created because I needed to debug some API's with
                the callback concept over the years (The problem is you can't call
                your local webserver most of the time because it's behind NAT and
                your development server is not configured to handle public traffic etc.)
            </p>

            <p>
                There is ofcourse alot of similar applications out there
                (alot of people run into the <i>"local dev server"</i>-problem).
                However. They where mostly annoying with trail periods, super
                advanced UI and limited amount of requests. etc. So i decided to
                make my own.
            </p>

            <p>
                Later this project evolved to be my
                <i>"try that new thing"</i>-project.
                So now, it serves as both a quick tool to check the HTTP request
                some API will send you and a place where i can try out new web technologies.
                And because the whole point of this application is that it's on a public webserver.
                Why not let others use it if they want!
            </p>

            <p>
                So if you want you can send me a <a href="mailto:henrik.hautakoski@gmail.com">email</a>
                if you find a bug, request some feature or just to let me know that i helped someone with debugging.
            </p>
        </div>
    </div>

    <div class="about-reference">

        <div class="section">

            <h4 class="text-center">Built with</h4>

            <div class="phalcon">
                <a target="_blank" href="http://phalconphp.com">
                    <img class="img-fluid" src="/img/phalcon-php.png" />
                </a>
            </div>

            <br/>

            <div class="text-center">
                <a target="_blank" href="http://getbootstrap.com">
                    <img height="40" src="/img/bootstrap-solid.svg" />
                    Bootstrap
                </a>
            </div>
        </div>
    </div>

</div>
