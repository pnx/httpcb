
<div class="section">

    <h1>Callback created</h1>

    <p>Set this link as callback url for the service you want to debug:</p>

    <strong>{{ serverUrl() }}{{ url(['for': 'cb-endpoint', 'id': id]) }}</strong>

    <a class="button button-default" href="{{ url('/callback/show/' ~ id) }}">
        {{ icon('solid/eye') }} View
    </a>

</div>
