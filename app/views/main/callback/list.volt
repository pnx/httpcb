
<div class="section">

    <div class="clearfix">
        <h2 class="float-start">Callbacks</h2>

        <div class="float-end">
            <a class="button button-large button-primary" href="{{ url('/callback/new') }}">
                {{ icon('solid/plus') }} New
            </a>
        </div>
    </div>

    {% if page.items|length > 0 %}

    <div class="callback-list">

        {% for item in page.items %}
        <div class="callback-list-item">

            <div class="callback-list-item-header">
                <a class="callback-list-item-name" href="/callback/show/{{ item.public_id }}">{{ item.name|e }}</a>

                {% if item.countRequests() > 0 %}
                <span class="badge badge-primary">
                    {{ item.countRequests() }} Requests
                </span>
                {% endif %}
            </div>

            <div class="callback-list-item-info">
                <span>{{ icon('clock') }} Created at: {{ item.created_at }}</span>

                <span>
                    {{ icon('paper-plane') }}

                    {% if item.countRequests() > 0 %}
                        Last request: {{ item.last_request }}
                    {% else %}
                        No requests yet.
                    {% endif %}
                </span>

                <span>
                    {{ icon('solid/link') }}
                    {{ serverUrl() }}{{ url(['for': 'cb-endpoint', 'id': item.public_id]) }}
                </span>
            </div>

            <a class="callback-list-item-arrow" href="/callback/show/{{ item.public_id }}">
                {{ icon('solid/arrow-alt-circle-right') }}
            </a>
        </div>
        {% endfor %}
    </div>

    {% else %}

    <div class="blankslate">
        <h3>No callbacks made yet.</h3>
        <p><a href="{{ url('/callback/new') }}">Create</a> a callback to begin!</p>
    </div>

    {% endif %}

    <nav class="text-center" aria-label="Page navigation">
        {{ partial('pagination') }}
    </nav>
</div>
