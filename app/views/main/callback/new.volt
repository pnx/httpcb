
<div class="section center-block" style="width: 400px">

    <h2>Create callback</h2>

    <form class="form-horizontal" method="post">
        <div class="row mb-3">
            <label class="col-sm-2 col-form-label text-end" for="name">Name</label>
            <div class="col-sm-10">
                {{ form.render('Name') }}
            </div>
        </div>
        <div class="row mb-3">
            <div class="offset-sm-2 col-sm-10">
                {{ form.render('Create') }}
            </div>
        </div>
    </form>
</div>
