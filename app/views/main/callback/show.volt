
<div class="section">
    <div class="clearfix">
        <h4 class="float-start">{{ item.name|e }}</h4>
        <h6 class="float-end"><strong>Created at:</strong> {{ item.created_at }}</h6>
    </div>

    <div class="panel-group request-list" id="request-list">

        {% for index, req in page.items %}

        <div class="panel request-list-item">
            <a id="request-item-head-{{ index }}" class="request-list-item-header collapsed"
               href="#request-item-body-{{ index }}" role="button" aria-controls="request-item-body-{{ index }}"
               data-bs-toggle="collapse" aria-expanded="true" >

                <div class="request-list-item-header-row">

                    <span class="request-list-item-header-method">
                        {{ req.getMethod() }}
                    </span>

                    <span class="request-list-item-header-uri">
                        <span class="url">{{ urlStyle(req.getUri()) }}</span>
                    </span>

                    <span class="request-list-item-header-timestamp">
                        {{ icon('clock') }} {{ req.getTimestamp() }}
                    </span>
                </div>

                <div class="request-list-item-header-row">

                    <span class="request-list-item-header-type">
                        {{ icon('file-alt') }} {{ req.getType() }}
                    </span>

                    <span class="request-list-item-header-size">
                        {{ icon('solid/database') }} {{ req.getSize() }} b
                    </span>

                    <span class="request-list-item-header-ip">
                        {{ icon('compass') }} {{ req.getSourceIp() }}
                    </span>

                </div>
            </a>

            <div id="request-item-body-{{ index }}" class="collapse" data-bs-parent="#request-list">

                <div class="request-list-item-detail">

                    {% if req.getUriQuery()|length > 0 %}

                    <button class="request-list-item-detail-button" type="button" role="button"
                            data-bs-toggle="collapse" data-bs-target="#request-list-item-detail-query-{{ index }}"
                            aria-expanded="false" aria-controls="request-list-item-detail-query-{{ index }}">
                        Query
                    </button>

                    <div id="request-list-item-detail-query-{{ index }}" class="collapse show">
                        <table class="request-list-item-detail-headers">
                            <thead>
                            <tr>
                                <th class="request-list-item-detail-headers-key">Key</th>
                                <th class="request-list-item-detail-headers-value">Value</th>
                            </tr>
                            </thead>
                            <tbody>
                            {% for key, val in req.getUriQuery() %}
                                <tr>
                                    <td><strong>{{ key|e }}</strong></td>
                                    <td>{{ val|e }}</td>
                                </tr>
                            {% endfor %}
                            </tbody>
                        </table>
                    </div>

                    {% endif %}

                    <button class="request-list-item-detail-button" type="button"
                            data-bs-toggle="collapse" data-bs-target="#request-list-item-detail-headers-{{ index }}"
                            aria-expanded="false" aria-controls="request-list-item-detail-headers-{{ index }}">
                        Headers
                    </button>

                    <div id="request-list-item-detail-headers-{{ index }}" class="collapse">
                        <table class="request-list-item-detail-headers">
                            <thead>
                            <tr>
                                <th class="request-list-item-detail-headers-key">Key</th>
                                <th class="request-list-item-detail-headers-value">Value</th>
                            </tr>
                            </thead>
                            <tbody>
                            {% for key, val in req.getHeaders() %}
                                <tr>
                                    <td><strong>{{ key|e }}</strong></td>
                                    <td>{{ val|e }}</td>
                                </tr>
                            {% endfor %}
                            </tbody>
                        </table>
                    </div>

                    <button class="request-list-item-detail-button" type="button"
                            data-bs-toggle="collapse" data-bs-target="#request-list-item-detail-body-{{ index }}"
                            aria-expanded="false" aria-controls="request-list-item-detail-body-{{ index }}">
                        Body
                    </button>

                    <div id="request-list-item-detail-body-{{ index }}" class="request-list-item-detail-body collapse in">

                        {% if (req.getBody()|length < 1) %}
                        <div class="blankslate blankslate-sm">
                            <h3>Empty body</h3>
                        </div>
                        {% else %}
                        <pre class="code"><code>{{ req.getBody() }}</code></pre>
                        {% endif %}
                    </div>
                </div>
            </div>
        </div>

        {% else %}
        <div class="blankslate">
            <h3>No requests made yet.</h3>
            <p>No http requests has been made to this callback.</p>
        </div>
        {% endfor %}

    </div>

    <nav class="text-center" aria-label="Page navigation">
        {{ partial('pagination') }}
    </nav>
</div>
