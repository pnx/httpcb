

<div class="login-container section">

    <h3>Login</h3>

    <div class="alert alert-info">
        <p class="text-center"><strong>Heads up!</strong> Signup is currently not available.</p>
        <p>
            Login using username/password can be setup after registration.
            First time users can only register with third party services below
        </p>
    </div>

    <span class="spacer"></span>

    <form class="form" method="post" action="">
        <div class="form-group">
            {{ form.render('Email') }}
        </div>
        <div class="form-group">
        {{ form.render('Password') }}
        </div>
        <div class="form-group">
            {{ form.render('Login') }}
        </div>
    </form>

    <span class="spacer"></span>

    <div class="oauth">

        <a class="button button-github" href="{{ url(['for': 'oauth', 'strategy': 'github']) }}">
            {{ icon('brand/github') }} GitHub
        </a>

        <a class="button button-google" href="{{ url(['for': 'oauth', 'strategy': 'google']) }}">
            {{ icon('brand/google') }} Google
        </a>

        <a class="button button-gitlab" href="{{ url(['for': 'oauth', 'strategy': 'gitlab']) }}">
            {{ icon('brand/gitlab') }} Gitlab
        </a>

        <a class="button button-linkedin" href="{{ url(['for': 'oauth', 'strategy': 'linkedin']) }}">
            {{ icon('brand/linkedin') }} LinkedIn
        </a>
    </div>
</div>

