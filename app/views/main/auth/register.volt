

<div class="register">

    <h2>Account registration</h2>

    <div class="alert alert-info alert-dismissible" role="alert">

        <strong>Information!</strong>

        <p>
            The form is prepared with the information provided by <strong>{{ provider }}</strong>.
            Please check the information and make changes if necessary before continue.
        </p>

    </div>

    <span class="spacer"></span>

    <form class="form form-horizontal" method="post">

        <div class="form-group">
            {{ form.renderDecorated('email') }}
        </div>

        <div class="form-group">
            {{ form.renderDecorated('username') }}
        </div>

        <div class="form-group">
            {{ form.renderDecorated('first-name', ['length': 4]) }}
            {{ form.renderDecorated('last-name', ['length': 4]) }}
        </div>

        <span class="spacer"></span>

        <div class="form-group">
            <div class="col-xs-12 col-xs-offset-2">
                {{ form.render('submit') }}
            </div>
        </div>
    </form>
</div>
