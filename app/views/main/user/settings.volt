{%
    set social_links = [
        'github'    : [ 'connected': user.getGithubId() > 0 ],
        'gitlab'    : [ 'connected': user.getGitlabId() > 0, 'class': 'text-gitlab' ],
        'google'    : [ 'connected': user.getGoogleId() > 0, 'class': 'text-google' ],
        'linkedin'  : [ 'connected': user.getLinkedinId() | length, 'class': 'text-linkedin' ]
    ]
%}

<div class="section">

    <form class="form-horizontal" method="post" action="">

        <div class="row mb-3">
            {{ form.renderDecorated('username', [ 'length': 7 ]) }}
            {{ form.renderDecorated('id', [ 'length': 2, 'label-length' : 1 ]) }}
        </div>

        <div class="row mb-3">
            {{ form.renderDecorated('name') }}
        </div>

        <div class="row mb-3">
            {{ form.renderDecorated('email') }}
        </div>

        <div class="row mb-3">
            <div class="col-sm-10 offset-sm-2">
                <h5>Password</h5>
            </div>
        </div>

        {% if form.has('passwordCurrent') %}
        <div class="row mb-3">
            {{ form.renderDecorated('passwordCurrent') }}
        </div>
        {% endif %}

        <div class="row mb-3">
            {{ form.renderDecorated('passwordNew') }}
        </div>

        <div class="row mb-3">
            {{ form.renderDecorated('passwordConfirm') }}
        </div>

        <div class="row mb-3">

            <div class="col-sm-10 offset-sm-2">

                <h5>Social sign-in</h5>

                <hr />

				<div class="row">
                {% for name,info in social_links %}
                <div class="col-sm-2 text-center">
                    {%  set class = info['class'] | default(false) %}
                    <div{{ class ? ' class="%s"'|format(class) : '' }}>{{ icon('brand/' ~ name, [ '3x' ]) }}</div>
                    {% if info['connected'] %}
                        <a href="{{ url(['for': 'oauth-disconnect', 'provider': name]) }}">Disconnect</a>
                    {% else %}
                        <a href="{{ url(['for': 'oauth', 'strategy': name]) }}">Connect</a>
                    {% endif %}
                </div>
                {% endfor %}
				</div>

            </div>
        </div>

        <div class="row mb-3">
            <div class="col-sm-10 offset-sm-2">
                <hr />
                {{ form.render('Save') }}

                <button class="button button-danger float-end" type="button" data-bs-toggle="modal" data-bs-target="#deleteModal">
                    Delete Account
                </button>
            </div>
        </div>

    </form>
</div>

<div class="modal fade" id="deleteModal" tabindex="-1" aria-labelledby="deleteModalLabel" aria-hidden="true">
    <div class="modal-dialog">
        <div class="modal-content">
            <div class="modal-header">
                <h4 class="modal-title float-start" id="deleteModalLabel">Delete account</h4>
                <button type="button" class="btn-close" data-bs-dismiss="modal" aria-label="Close"></button>
            </div>

            <form method="post" action="/user/delete">
                <div class="modal-body">
                    <p>
                        Deleting your account is a non-reversible action.
                        All data associated with your account will be lost in the process.
                    </p>
                    {% if user.password|length > 0 %}
                    <p>Enter your <kbd>password</kbd> to confirm:</p>

                    <input type="password" name="currentpw" class="form-control" />
                    {% endif %}
                </div>

                <div class="modal-footer">
                    <button type="button" class="button button-default" data-bs-dismiss="modal">Close</button>
                    <input type="submit" name="deleteAcc" class="button button-danger" value="Delete account">
                </div>
            </form>
        </div>
    </div>
</div>
