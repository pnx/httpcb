
<div class="section">

    <h3>Activity Log</h3>

    <table class="table table-condensed table-striped table-hover">
        <thead>
            <tr>
                <th>Date</th>
                <th>Ip</th>
                <th>Message</th>
            </tr>
        </thead>

        <tbody>
        {% for item in page.items %}
            <tr>
                <td>{{ item.getTimestamp() }}</td>
                <td>{{ item.getIp() }}</td>
                <td>{{ item.getMessage() }}</td>
            </tr>
        {% endfor %}
        </tbody>
    </table>

    <nav class="text-center" aria-label="Page navigation">
        {{ partial('pagination') }}
    </nav>
</div>


