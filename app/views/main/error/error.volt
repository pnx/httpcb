
<span class="text-center">
    <h1>An Error Occurred!</h1>

    <p>The server freaked out while computing the bits. Try again later.</p>

    <p>If the problem persist, contact <a href="mailto:henrik.hautakoski@gmail.com">this guy</a></p>
</span>
