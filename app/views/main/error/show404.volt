
{% set google_url = "http://google.com?q=" ~ url | escape %}

<span class="text-center">
    <h1>404 Not Found</h1>
    <p>
        This page does not exist! You can search for it on
        <a target="_blank" href="{{ google_url }}">google</a>
        if you want.
    </p>
</span>
