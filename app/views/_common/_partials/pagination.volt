
{%- macro max(a, b) %}
    {% return a > b ? a : b %}
{%- endmacro %}

{%- macro min(a, b) %}
    {% return a < b ? a : b %}
{%- endmacro %}

{% set pagination_slider = 3 %}

{% if (page.last > 1) %}

<ul class="pagination">

    {% if page.current !== page.previous %}
    <li>
        <a href="{{ pagination_url ~ page.previous }}">
            {{ icon('solid/arrow-left') }} Previous
        </a>
    </li>
    {% endif %}

    {% if page.last > pagination_slider and page.current > (pagination_slider + 1) %}
    <li>
        <a href="{{ pagination_url ~ 1 }}">1</a>
    </li>
    <li class="middle">
        ...
    </li>
    {% endif %}

    {% for n in max(page.current - pagination_slider, 1)..min(page.current + pagination_slider, page.last) %}
        {% if (n == page.current) %}
            <li class="active">
        {% else %}
            <li>
        {% endif %}
                <a href="{{ pagination_url ~ n }}">{{ n }}</a>
            </li>
    {% endfor %}

    {% if page.last > pagination_slider and page.current < page.last - pagination_slider %}
    <li class="middle">
        ...
    </li>
    <li>
        <a href="{{ pagination_url ~ page.last }}">{{ page.last }}</a>
    </li>
    {% endif %}

    {% if page.current !== page.next %}
    <li>
        <a href="{{ pagination_url ~ page.next }}">
            Next {{ icon('solid/arrow-right') }}
        </a>
    </li>
    {% endif %}
</ul>

{% endif %}
