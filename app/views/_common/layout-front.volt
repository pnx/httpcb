
{% extends 'layout.volt' %}

{% block masthead %}

<div class="masthead">

	<div class="logo"></div>

    <h1>Welcome to HTTP Callback</h1>

    <p>
        This tool is created to help developers integrate
        API's that uses HTTP Callbacks. Give it a go!
    </p>

    <p>
        Find out what HTTP Callback can do for you today!
    </p>

    <a class="button button-large masthead-get-started-button" href="/callback/new">Get started</a>

</div>

{% endblock %}
