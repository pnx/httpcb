
<footer class="footer">
    <div class="footer-left">
        Copyright &copy; 2017 - 2022
        <a target="_blank" href="http://www.shufflingpixels.com">
            Shufflingpixels
        </a>
    </div>

    <div class="footer-middle">
        <a class="footer-button-top" href="#top">
            {{ icon('solid/caret-up') }}
        </a>
    </div>

    <div class="footer-right">
        Version 1.1
    </div>
</footer>
