
{% set types = {
    'error': 'danger',
    'success': 'success',
    'notice': 'info',
    'warning': 'warning'
} %}

{% if (flash.has()) %}
    {% for type, messages in flash.getMessages() %}
        {% for message in messages %}
            <div class="alert alert-{{ types[type] }} alert-dismissible" role="alert">
				<h5 class="alert-heading">{{ type|capitalize }}</h5>
                <p class="mb-0">{{ message }}</p>
				<button type="button" class="btn-close" data-bs-dismiss="alert" aria-label="Close">
            </div>
        {% endfor%}
    {% endfor %}
{% endif %}
