
<div class="navigation" role="navigation">

    <button class="menu-button" type="button" data-toggle="collapse" data-target="#main-menu" aria-expanded="false" aria-label="Toggle navigation">
        {{ icon('solid/bars') }}
    </button>

    <div class="navigation-user-menu">
    {% if auth.hasIdentity() %}
        <div class="navigation-user-menu-dropdown">
            <a id="user-dropdown-button" class="navigation-user-menu-dropdown-button"
               data-bs-toggle="dropdown" role="button" aria-expanded="false">

                {{ icon('solid/user') }} <strong>{{ auth.getUser().username }}</strong>
                {% set imp = auth.getImpersonator() %}
                {% if imp %}( {{ icon('solid/user-secret') }} {{ imp.username }} ){% endif %}
            </a>

            <ul class="dropdown-menu navigation-user-menu-dropdown-list">
                <li>{{ link_to(['for': 'user-settings'], '<i class="icon fas fa-cog"></i> Settings') }}</li>
                <li>{{ link_to('/user/activity', '<i class="icon fas fa-list-alt"></i> Activity') }}</li>
                <li><hr class="dropdown-divider"></li>
                <li>{{ link_to(['for': 'logout'], '<i class="icon far fa-times-circle"></i> Log out') }}</li>
            </ul>
        </div>
    {% else %}
        <div class="navigation-user-menu-login">{{ link_to(['for': 'login'], '<i class="icon far fa-arrow-alt-circle-right"></i> Login', 'class': 'login-button') }}</div>
    {% endif %}
    </div>

    <nav class="navigation-menu collapse" id="main-menu">
        {{ menu.render(0) }}
    </nav>
</div>
