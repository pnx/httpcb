<!DOCTYPE html>
<html>
    <head>
        {{ assets.outputCss() }}

        <meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
		<link rel="apple-touch-icon" sizes="180x180" href="{{ url('/apple-touch-icon.png') }}">
		<link rel="icon" type="image/png" sizes="32x32" href="{{ url('/favicon-32x32.png') }}">
		<link rel="icon" type="image/png" sizes="16x16" href="{{ url('/favicon-16x16.png') }}">
		<link rel="manifest" href="{{ url('/site.webmanifest') }}">
        <title>HTTP Callback</title>
    </head>

    <body>
        {% block body %}{% endblock %}

        <header class="head-section">
            <div class="top-section">
                {% include "_components/navigation.volt" %}
            </div>

            {% block masthead %}{% endblock %}
        </header>

        <main class="content-section">

            {% include "_components/flash.volt" %}

            {{ content() }}

        </main>

        <div class="footer-section">
            {% include "_components/footer.volt" %}
        </div>

        {{ assets.outputJs() }}
    </body>
</html>
