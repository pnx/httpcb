
<div class="backend">

    <ul class="backend-sidemenu">
        <li><a href="{{ url('/admin') }}">
            {{ icon('solid/users') }} Users
        </a></li>
        <li><a href="{{ url('/admin/log') }}">
            {{ icon('solid/bars') }} Log
        </a></li>
    </ul>

    <div class="backend-content">
        {{ content() }}
    </div>
</div>



