
<h1>Activity Log</h1>

<table class="table table-condensed table-striped table-hover">
    <thead>
    <tr>
        <th>Date</th>
        <th>Ip</th>
        <th>User</th>
        <th>Message</th>
    </tr>
    </thead>

    <tbody>
    {% for item in page.items %}
        <tr>
            <td>{{ item.getTimestamp() }}</td>
            <td>{{ item.getIp() }}</td>
            <td>
                {% if item.getUser() %}
                {{ item.getUser().getId() }}:{{ item.getUser().getUsername() }}
                {% else %}
                    -
                {% endif %}
            </td>
            <td>{{ item.getMessage() }}</td>
        </tr>
    {% endfor %}
    </tbody>
</table>

<nav class="text-center" aria-label="Page navigation">
    {{ partial('pagination') }}
</nav>
