
<div class="clearfix">
    <h1 class="float-start">Users</h1>

    <div class="float-end">
        <a class="button button-large button-primary" href="{{ url(['for': 'backend-user-new' ]) }}">
            {{ icon('solid/plus') }} New
        </a>
    </div>
</div>

<table class="table table-striped table-hover">

    <thead>
        <tr>
            <th>&nbsp;</th>
            <th>#</th>
            <th>Username</th>
            <th>Name</th>
            <th>Email</th>
            <th>Type</th>
            <th>Status</th>
            <th>&nbsp;</th>
        </tr>
    </thead>

    <tbody>
    {% for item in page.items %}
        <tr>
            <td>
                <a title="Edit" href="{{ url(['for': 'backend-user-edit', 'id': item.id ]) }}">
                    {{ icon('solid/pen') }}
                </a>
            </td>
            <td>{{ item.id }}</td>
            <td>{{ item.username }}</td>
            <td>{{ item.name }}</td>
            <td>{{ item.email }}</td>
            <td>{{ item.type | capitalize }}</td>
            <td><span class="badge {{ item.isActive() ? 'badge-success' : 'badge-danger' }}">{{ item.status }}</span></td>
            <td>
                <a title="Impersonate" href="{{ url(['for': 'backend-user-impersonate', 'id': item.id ]) }}">
                    {{ icon('solid/user-secret') }}
                </a>
            </td>
        </tr>
    {% endfor %}
    </tbody>
</table>

<nav class="text-center" aria-label="Page navigation">
    {{ partial('pagination') }}
</nav>
