
<div class="clearfix">
    <h2 class="float-start">
        {% if (user.getId()) %} Edit user #{{ user.getId() }} {% else %} Create user {% endif %}
    </h2>

{% if (user.getId() and user.isActive() === false) %}
    <p class="float-end badge badge-danger">{{ user.getStatus() }}</p>
{% endif %}

</div>

<form class="form-horizontal" method="post" action="">

    <div class="row mb-3">
        {% if (form.has('id')) %}
        {{ form.renderDecorated('username', [ 'length': 7 ]) }}
        {{ form.renderDecorated('id', [ 'length': 2, 'label-length' : 1 ]) }}
        {% else %}
        {{ form.renderDecorated('username') }}
        {% endif %}
    </div>

    <div class="row mb-3">
        {{ form.renderDecorated('name') }}
    </div>

    <div class="row mb-3">
        {{ form.renderDecorated('email') }}
    </div>

    <div class="row mb-3">
        <div class="offset-sm-2 col-sm-10">
            <h4>Password</h4>
        </div>
    </div>

    <div class="row mb-3">
        {{ form.renderDecorated('passwordNew') }}
    </div>

    <div class="row mb-3">
        {{ form.renderDecorated('passwordConfirm') }}
    </div>

    <div class="row mb-3">
        <div class="offset-sm-2 col-sm-10">
            <hr />
            {% if (form.has('Save')) %}
            {{ form.render('Save') }}
            {% else %}
            {{ form.render('Create') }}
            {% endif %}


            {% if (user.getId()) %}
            {% set actions = [ 'Activate': 'Active', 'Suspend': 'Suspended', 'Delete': 'Deleted' ] %}
            <div class="float-end">

                {% if user.isSuspended() %}
                <a class="button button-info" href="{{ url(['for': 'backend-user-activation-email', 'id': user.getId() ]) }}">
                    Send activation email
                </a>
                {% endif %}

                {% for label, status in actions %}

                {% if (user.status != status) %}
                <a class="button button-{{ status == 'Active' ? 'success' : 'danger' }}"
                   href="{{ url(['for': 'backend-user-status', 'type': status, 'id': user.getId() ]) }}">
                    {{ label }}
                </a>
                {% endif %}

                {% endfor %}
            </div>
            {% endif %}
        </div>
    </div>

</form>
