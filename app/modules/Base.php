<?php

namespace App\Module;

use Phalcon\Loader,
    Phalcon\Di\DiInterface,
    Phalcon\Mvc\Dispatcher,
    Phalcon\Mvc\ModuleDefinitionInterface;

abstract class Base implements ModuleDefinitionInterface
{
    protected $_controllerPath;

    protected $_controllerNamespace;

    protected $_viewDir;

    /**
     * Register a specific autoloader for the module
     */
    public function registerAutoloaders(DiInterface $di = null)
    {
        $loader = new Loader();

        $loader->registerNamespaces([
            $this->_controllerNamespace => $this->_controllerPath,
        ]);

        $loader->register();
    }

    /**
     * Register specific services for the module
     */
    public function registerServices(DiInterface $di)
    {
        $dispatcher = $di->get('dispatcher');

        $dispatcher->setDefaultNamespace($this->_controllerNamespace);

        $di->get('view')->setViewsDir(array_merge(
            [$this->_viewDir],
            (array) $di->get('view')->getViewsDir()
        ));
    }
}
