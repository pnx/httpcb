<?php

namespace App\Module;

class Backend extends Base
{
    protected $_controllerPath = APP_PATH . '/app/controllers/backend';

    protected $_controllerNamespace = 'App\Controller\Backend';

    protected $_viewDir = '../app/views/backend/';
}
