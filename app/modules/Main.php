<?php

namespace App\Module;

class Main extends Base
{
    protected $_controllerPath = APP_PATH . '/app/controllers';

    protected $_controllerNamespace = 'App\Controller';

    protected $_viewDir = '../app/views/main/';
}
