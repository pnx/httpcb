<?php

namespace App\Form;

/**
 * Models
 */

use App\Model\Data\User as UserModel;

/**
 * Form
 */

use Httpcb\Form as FormBase;

/**
 * Element types
 */

use Phalcon\Forms\Element\Text,
    Phalcon\Forms\Element\Password,
    Phalcon\Forms\Element\Submit;

/**
 * Validators
 */

use Phalcon\Validation\Validator\Callback as CallbackValidator,
    Phalcon\Validation\Validator\Uniqueness as UniquenessValidator,
    Phalcon\Validation\Validator\Alnum as AlnumValidator,
    Phalcon\Validation\Validator\PresenceOf as PresenceOfValidator,
    Phalcon\Validation\Validator\Email as EmailValidator,
    Phalcon\Validation\Validator\Confirmation as ConfirmationValidator,
    Phalcon\Validation\Validator\StringLength as StringLengthValidator,
    Phalcon\Validation\Validator\Identical as IdenticalValidator,
    Httpcb\Validation\Validator\Alpha as AlphaValidator;


class UserSettings extends FormBase
{
    /**
     * @var bool
     */
    protected $_admin;

    public function __construct(UserModel $user = null, bool $admin = false)
    {
        $this->_admin = $admin;

        parent::__construct($user);
    }

    public function initialize()
    {
        $entity = $this->getEntity();

        $this->setValidation(new \Phalcon\Validation());

        // Id
        if ($entity && $entity->getId()) {

            $id = new Text('id', array(
                'class' => 'form-control',
                'readonly' => '',
                'disabled' => 'disabled',
            ));

            $id->addValidator(new IdenticalValidator([
                'accepted' => $entity->getId(),
                'allowEmpty' => true
            ]));

            $id->setLabel('ID');
            $this->add($id);
        }

        // Username
        $username = new Text('username', array(
            'class' => 'form-control',
            'placeholder' => 'Username',
        ));

        $username->setLabel('Username');

        $validator_options = array(
            'model'     => new UserModel(),
            'message'   => 'The :field already exists.',
            'attribute' => 'username',
        );

        if ($entity && strlen($entity->getUsername())) {
            $validator_options['except'] = [$entity->getUsername()];
        }

        $username->addValidators([
            new AlnumValidator(),
            new UniquenessValidator($validator_options)
        ]);

        $this->add($username);

        // Name
        $name = new Text('name', array(
            'class' => 'form-control',
            'placeholder' => 'Name',
        ));

        $name->setLabel('Name');
        $name->addValidator(new AlphaValidator([
            'allowSpace' => true,
            'allowEmpty' => true,
        ]));

        $this->add($name);

        // Email
        if ($this->_admin === false && $entity) {
            $email = new Text('email', array(
                'class' => 'form-control',
                'placeholder' => 'Email',
                'readonly' => '',
                'disabled' => 'disabled',
            ));

            $email->addValidator(new IdenticalValidator([
                'accepted' => $entity->getEmail(),
                'allowEmpty' => true
            ]));
        } else {
            $email = new Text('email', array(
                'class' => 'form-control',
                'placeholder' => 'Email',
            ));

            $validator_options = [
                'model'     => new UserModel(),
                'message'   => 'The :field already exists.',
                'attribute' => 'email',
            ];

            if ($entity && strlen($entity->getEmail())) {
                $validator_options['except'] = [$entity->getEmail()];
            }

            $email->addValidators([
                new EmailValidator(),
                new UniquenessValidator($validator_options)
            ]);
        }

        $email->setLabel('Email');
        $this->add($email);

        // Passwords
        $this->_passwords();

        // Submit
        $submit = new Submit($entity && $entity->getId() ? 'Save' : 'Create', array('class' => 'button button-default'));
        $this->add($submit);
    }

    /**
     * Password section
     */
    protected function _passwords()
    {
        $entity = $this->getEntity();

        // Current
        if ($this->_admin === false && $entity && strlen($entity->getPassword()) > 0) {
            $current = new Password('passwordCurrent', array(
                'class' => 'form-control',
            ));
            $current->setLabel('Current password');
            $this->add($current);
        }

        // New
        $new = new Password('passwordNew', array(
            'class' => 'form-control',
        ));
        $new->setLabel('New password');
        $this->add($new);

        // Confirm
        $confirm = new Password('passwordConfirm', array(
            'class' => 'form-control',
        ));
        $confirm->setLabel('Confirm');
        $this->add($confirm);

        // Validation
        $validation = $this->getValidation();

        if ($this->_admin === false && $entity && strlen($entity->getPassword()) > 0) {
            $validation->add('passwordCurrent', new CallbackValidator([
                'callback' => function ($data) {
                    $new_pw = $data['passwordNew'];
                    if (strlen($new_pw) > 0) {
                        $value = $data['passwordCurrent'];
                        $hash = $this->getEntity()->getPassword();

                        // Only fail if there is a password and they did not match.
                        if (strlen($hash) > 0 && password_verify($value, $hash) === false) {
                            return false;
                        }
                    }
                    return true;
                },
                'message' => 'Password is not valid.'
            ]));
        }

        $validation->add('passwordNew', new StringLengthValidator([
            'allowEmpty' => true,
            'min' => 8,
            'messageMinimum' => 'Password must be atleast 8 characters long',
        ]));

        $validation->add('passwordConfirm', new ConfirmationValidator([
            'message' => 'Passwords does not match',
            'with' => 'passwordNew',
        ]));
    }
}
