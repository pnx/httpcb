<?php

namespace App\Form;

/**
 * Models
 */

use App\Model\Data\User;

/**
 * Phalcon Form
 */

use Httpcb\Form as FormBase;

/**
 * Element types
 */

use Phalcon\Forms\Element\Text,
    Phalcon\Forms\Element\Password,
    Phalcon\Forms\Element\Submit;

/**
 * Validators
 */

use Phalcon\Validation,
    Phalcon\Validation\Validator\Callback as CallbackValidator,
    Phalcon\Validation\Validator\Alnum as AlnumValidator,
    Phalcon\Validation\Validator\Email as EmailValidator,
    Phalcon\Validation\Validator\StringLength as StringLengthValidator,
    Phalcon\Validation\Validator\Identical as IdenticalValidator,
    Httpcb\Validation\Validator\Alpha as AlphaValidator;


class Registration extends FormBase
{
    public function initialize(User $user)
    {
        $this->setValidation(new Validation());

        // Username
        $username = new Text('username', array(
            'class' => 'form-control',
            'placeholder' => 'Username',
        ));

        $username->setLabel('Username');

        $username->addValidators([
            new AlnumValidator([
                'message' => 'Username must contain only letters and numbers.'
            ]),
            new StringLengthValidator([
                'min'            => 2,
                'messageMinimum' => 'Username must be at least :min characters long.',
            ]),
            new CallbackValidator([
                'callback'  => function ($data) {
                    return User::findFirstByUsername($data['username']) === false;
                },
                'message'   => 'The username already exists.',
                'attribute' => 'username',
            ])
        ]);

        $this->add($username);

        // Names
        foreach (['first-name' => 'Firstname', 'last-name' => 'Lastname'] as $id => $label) {

            $name = new Text($id, array(
                'class' => 'form-control',
                'placeholder' => $label,
            ));

            $name->setLabel($label);
            $name->addValidator(new AlphaValidator([
                'allowSpace' => false,
                'allowEmpty' => true,
            ]));

            $this->add($name);
        }

        // Email
        $email = new Text('email', array(
            'class' => 'form-control',
            'placeholder' => 'Email'
        ));

        $email->addValidators([
            new CallbackValidator([
                'callback'  => function ($data) {
                    return User::findFirstByEmail($data['email']) === false;
                },
                'message'   => 'This email already exist.',
            ])
        ]);

        $email->setLabel('Email');
        $this->add($email);

        // Submit
        $submit = new Submit('submit', array('class' => 'button button-success', 'value' => 'Register'));
        $this->add($submit);
    }
}
