<?php

namespace App\Form;

use Phalcon\Forms\Form;

/**
 * Element types
 */

use Phalcon\Forms\Element\Text;
use Phalcon\Forms\Element\Password;
use Phalcon\Forms\Element\Submit;

/**
 * Validators
 */

use Phalcon\Validation\Validator\PresenceOf;
use Phalcon\Validation\Validator\Email as EmailValidator;
use Phalcon\Validation\Validator\StringLength;

class Login extends Form
{
    public function initialize()
    {
        $this->setEntity($this);

        // Email
        $email = new Text('Email', array(
            'class' => 'form-control',
            'placeholder' => 'Username/Email',
        ));

        $this->add($email);

        // Password
        $passwd = new Password('Password', array(
            'class' => 'form-control',
            'placeholder' => 'Password',
        ));

        $validator = new StringLength(array(
            'min' => 8,
            'messageMinimum' => 'Password must be atleast 8 characters long',
        ));
        $passwd->addValidator($validator);

        $this->add($passwd);

        // Submit

        $submit = new Submit('Login', array('class' => 'button button-default button-block'));
        $this->add($submit);
    }
}
