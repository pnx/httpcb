<?php

namespace App\Form;

use Phalcon\Forms\Form;

/**
 * Element types
 */

use Phalcon\Forms\Element\Text;
use Phalcon\Forms\Element\Submit;

/**
 * Validators
 */

use Phalcon\Validation\Validator\StringLength;

class CallbackCreate extends Form
{
    public function initialize()
    {
        $this->setEntity($this);
        // Name
        $name = new Text('Name', array(
            'class' => 'form-control',
        ));

        $validator = new StringLength([
            'max' => 50,
            'min' => 2,
            'messageMaximum' => 'Must be less than :max characters.',
            'messageMinimum' => 'Must be atleast :min characters long.'
        ]);

        $name->addValidator($validator);
        $this->add($name);

        // Submit

        $submit = new Submit('Create', array('class' => 'button button-brand'));
        $this->add($submit);
    }
}
