
{% set link_url = request.getScheme() ~ '://'
    ~ request.getHttpHost()
    ~ url(['for': 'activation-link', 'link': link ]) %}

<h1>Httcb Password Activation</h1>

<p>Please click <a href="{{ link_url }}">here</a> to activate the password.</p>

<p style="color:grey">Or copy and paste this link into the address field in your browser: <i>{{ link_url }}</i></p>
