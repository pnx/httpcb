<?php

namespace App\Model\Data;

use Phalcon\Validation,
    Phalcon\Validation\Validator\Callback as CallbackValidator,
    InvalidArgumentException,
    Httpcb\OAuth\UserData\UserDataInterface;

class User extends Base
{
    const TYPE_USER             = 'user';
    const TYPE_ADMIN            = 'admin';

    const STATUS_ACTIVE         = 'Active';
    const STATUS_DELETED        = 'Deleted';
    const STATUS_SUSPENDED      = 'Suspended';

    protected $id;

    protected $username;

    protected $firstname;

    protected $lastname;

    protected $email;

    protected $type;

    protected $status;

    protected $password;

    protected $github_id;

    protected $gitlab_id;

    protected $google_id;

    protected $linkedin_id;

    public function initialize()
    {
        $this->useDynamicUpdate(true);

        // Keep snapshots so we know if something changes.
        $this->keepSnapshots(true);

        // Set default event manager
        $this->setEventsManager($this->getDI()->get('eventsManager'));
    }

    public function validation()
    {
        $rules = [
            'username' => new CallbackValidator([
                'callback' => function () {
                    return $this->findFirstByUsername($this->getUsername()) === false;
                },
                'message' => 'The username already exists.'
            ]),
            'email' => new CallbackValidator([
                'callback' => function () {
                    return $this->findFirstByEmail($this->getEmail()) === false;
                },
                'message' => 'The email address already exists.'
            ])
        ];

        $validation = new Validation();
        foreach ($rules as $field => $validator) {

            // Only validate changed fields.
            if ($this->hasChanged($field)) {
                $validation->add($field, $validator);
            }
        }

        return $this->validate($validation);
    }

    /**
     * @return mixed
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * @param mixed $id
     * @return User
     */
    public function setId($id)
    {
        $this->id = $id;
        return $this;
    }

    /**
     * @return mixed
     */
    public function getUsername()
    {
        return $this->username;
    }

    /**
     * @param mixed $username
     * @return User
     */
    public function setUsername($username)
    {
        $this->username = $username;
        return $this;
    }

    /**
     * @return string
     */
    public function getName()
    {
        $name = $this->getFirstname();
        if (strlen($this->getLastname())) {
            $name .= ' ' . $this->getLastname();
        }
        return $name;
    }

    /**
     * @param string $name
     * @return User
     */
    public function setName($name)
    {
        $firstname = $name;
        $lastname = null;

        $pos = strpos($name, ' ');
        if ($pos !== false) {
            $firstname = substr($name, 0, $pos);
            $lastname = substr($name, $pos + 1);
        }

        $this->setFirstname($firstname);
        $this->setLastname($lastname);
        return $this;
    }

    /**
     * @return string|null
     */
    public function getFirstname()
    {
        return $this->firstname;
    }

    /**
     * @param string $firstname
     * @return User
     */
    public function setFirstname($firstname)
    {
        if ($firstname !== null) {
            $firstname = (string) $firstname;
        }
        $this->firstname = $firstname;
        return $this;
    }

    /**
     * @return string|null
     */
    public function getLastname()
    {
        return $this->lastname;
    }

    /**
     * @param string|null $lastname
     * @return User
     */
    public function setLastname($lastname)
    {
        if ($lastname !== null) {
            $lastname = (string) $lastname;
        }
        $this->lastname = $lastname;
        return $this;
    }

    /**
     * @return mixed
     */
    public function getEmail()
    {
        return $this->email;
    }

    /**
     * @param mixed $email
     * @return User
     */
    public function setEmail($email)
    {
        $this->email = $email;
        return $this;
    }

    /**
     * @return string
     */
    public function getType()
    {
        return $this->type;
    }

    /**
     * @param string $type
     * @return User
     */
    public function setType($type)
    {
        $this->type = $type;
        return $this;
    }

    /**
     * @return string
     */
    public function getStatus()
    {
        return $this->status;
    }

    /**
     * @param string $value
     * @return User
     */
    public function setStatus($value)
    {
        $allowed_values = array(
            self::STATUS_ACTIVE,
            self::STATUS_DELETED,
            self::STATUS_SUSPENDED
        );

        if (!in_array($value, $allowed_values)) {
            $msg = "Status '{$value}' is not a valid enum value'";
            throw new InvalidArgumentException($msg);
        }

        $this->status = $value;
        return $this;
    }

    /**
     * Returns true if this is a active user.
     *
     * @return bool
     */
    public function isActive()
    {
        return $this->status == self::STATUS_ACTIVE;
    }

    /**
     * Returns true if this user is suspended.
     *
     * @return bool
     */
    public function isSuspended()
    {
        return $this->status == self::STATUS_SUSPENDED;
    }

    /**
     * @return mixed
     */
    public function getPassword()
    {
        return $this->password;
    }

    /**
     * @param mixed $password
     * @return User
     */
    public function setPassword($password)
    {
        $this->password = $password;
        return $this;
    }

    /**
     * @return mixed
     */
    public function getGithubId()
    {
        return $this->github_id;
    }

    /**
     * @param mixed $github_id
     * @return User
     */
    public function setGithubId($github_id)
    {
        $this->github_id = $github_id;
        return $this;
    }

    /**
     * @return mixed
     */
    public function getGitlabId()
    {
        return $this->gitlab_id;
    }

    /**
     * @param mixed $gitlab_id
     * @return User
     */
    public function setGitlabId($gitlab_id)
    {
        $this->gitlab_id = $gitlab_id;
        return $this;
    }

    /**
     * @return mixed
     */
    public function getGoogleId()
    {
        return $this->google_id;
    }

    /**
     * @param mixed $google_id
     * @return User
     */
    public function setGoogleId($google_id)
    {
        $this->google_id = $google_id;
        return $this;
    }

    /**
     * @return string|null
     */
    public function getLinkedinId()
    {
        return $this->linkedin_id;
    }

    /**
     * @param string $provider
     * @param string $id
     * @return $this
     */
    public function setOAuthId($provider, $id)
    {
        $method = 'set' . ucfirst($provider) . 'Id';
        if (method_exists($this, $method)) {
            $this->$method($id);
        }
        return $this;
    }

    /**
     * @param string $id
     * @return User
     */
    public function setLinkedinId($id)
    {
        $this->linkedin_id = (string) $id;
        return $this;
    }

    public function getSocialLinks()
    {
        $providers = [
            'github'    => $this->getGithubId(),
            'gitlab'    => $this->getGitlabId(),
            'google'    => $this->getGoogleId(),
            'linkedin'  => $this->getLinkedinId()
        ];

        return array_filter($providers);
    }

    static public function createFromOAuthData(UserDataInterface $data)
    {
        $oauth_id = 'set' . $data->getProvider() . 'Id';

        $user = new self();
        $user->setUsername($data->getUsername())
            ->setName($data->getName())
            ->setEmail($data->getEmail())
            ->{$oauth_id}($data->getId());

        return $user;
    }

    /**
     * Find the first user by Username or Email
     *
     * @param string $value
     * @return \Phalcon\Mvc\Model
     */
    static public function findFirstByUsernameOrEmail($value)
    {
        return self::findFirst([
            "(email = :v: OR username = :v:) AND status != :s:",
            "bind" => ['v' => $value, 's' => self::STATUS_DELETED]
        ]);
    }

    static public function findFirstByEmail($email)
    {
        return self::findFirst([
            "email = :email: AND status != :s:",
            "bind" => ['email' => $email, 's' => self::STATUS_DELETED]
        ]);
    }

    static public function findFirstByUsername($username)
    {
        return self::findFirst([
            "username = :username: AND status != :s:",
            "bind" => ['username' => $username, 's' => self::STATUS_DELETED]
        ]);
    }

    static public function findFirstByOAuthID(UserDataInterface $oauth)
    {
        $column = strtolower($oauth->getProvider());

        return self::findFirst([
            "{$column}_id = :id: AND status != :s:",
            "bind" => ['id' => $oauth->getId(), 's' => self::STATUS_DELETED]
        ]);
    }

    public function beforeSave()
    {
        // Fire event on password create/changed.
        $manager = $this->getEventsManager();

        // EventManager exist
        if ($manager) {

            // If we have Snapshot data (existing row)
            if ($this->hasSnapshotData()) {
                $has_new_value = $this->hasChanged('password');
                $old_value = $this->getOldSnapshotData()['password'];
            }
            // New row, check if we have a password.
            else {
                $has_new_value = strlen($this->password) > 0;
                $old_value = null;
            }

            // we have a new password.
            if ($has_new_value) {

                // Empty password before
                if (strlen($old_value) < 1) {
                    $manager->fire('user:onPasswordCreated', $this);
                } else {
                    $manager->fire('user:onPasswordChanged', $this);
                }
            }
        }
    }

    /**
     * @param int $page
     * @param int $limit
     * @return \Phalcon\Paginator\AdapterInterface
     */
    public static function getPaginationList($page = 1, $limit = 30)
    {
        $builder = (new self())->getModelsManager()->createBuilder();

        $builder->from(self::class);

        $paginator = new \Phalcon\Paginator\Adapter\QueryBuilder(array(
            'builder'   => $builder,
            'page'      => $page,
            'limit'     => $limit
        ));

        return $paginator;
    }
}
