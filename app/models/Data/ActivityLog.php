<?php

namespace App\Model\Data;

use Phalcon\Mvc\Model\Query\BuilderInterface,
    Phalcon\Paginator\Adapter\QueryBuilder;

class ActivityLog extends Base
{
    protected $id;

    protected $timestamp;

    protected $user_id;

    protected $ip;

    protected $message;

    /**
     * Initialize method for model.
     */
    public function initialize()
    {
        // Relationships
        $this->hasOne('user_id', User::class, 'id', ['alias' => 'User']);
    }

    /**
     * @return mixed
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * @param mixed $id
     * @return User
     */
    public function setId($id)
    {
        $this->id = $id;
        return $this;
    }

    /**
     * @return mixed
     */
    public function getTimestamp()
    {
        return $this->timestamp;
    }

    /**
     * @param mixed $timestamp
     * @return ActivityLog
     */
    public function setTimestamp($timestamp)
    {
        $this->timestamp = $timestamp;
        return $this;
    }

    /**
     * @return mixed
     */
    public function getUserId()
    {
        return $this->user_id;
    }

    /**
     * @param mixed $user_id
     * @return ActivityLog
     */
    public function setUserId($user_id)
    {
        $this->user_id = $user_id;
        return $this;
    }

    /**
     * @return mixed
     */
    public function getIp()
    {
        return $this->ip;
    }

    /**
     * @param mixed $ip
     * @return ActivityLog
     */
    public function setIp($ip)
    {
        $this->ip = $ip;
        return $this;
    }

    /**
     * @return mixed
     */
    public function getMessage()
    {
        return $this->message;
    }

    /**
     * @param mixed $message
     * @return ActivityLog
     */
    public function setMessage($message)
    {
        $this->message = $message;
        return $this;
    }

    /**
     * @param $userid
     * @param int $page
     * @param int $limit
     * @return \Phalcon\Paginator\AdapterInterface
     */
    public static function getPaginationList($userid, $page = 1, $limit = 30)
    {
        $builder = (new self())->getModelsManager()->createBuilder();

        $builder->from(self::class)
            ->where('user_id = :uid:', array('uid' => $userid))
            ->orderBy('timestamp DESC');

        return self::_paginate($builder, $page, $limit);
    }

    /**
     * @param int $page
     * @param int $limit
     * @return \Phalcon\Paginator\AdapterInterface
     */
    public static function getAllPaginationList($page = 1, $limit = 30)
    {
        $builder = (new self())->getModelsManager()->createBuilder();

        $builder->from(self::class)
            ->orderBy('timestamp DESC');

        return self::_paginate($builder, $page, $limit);
    }

    protected static function _paginate(BuilderInterface $builder, $page = 1, $limit = 30)
    {
        $paginator = new QueryBuilder(array(
            'builder'   => $builder,
            'page'      => $page,
            'limit'     => $limit
        ));

        return $paginator;
    }
}
