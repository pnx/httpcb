<?php

namespace App\Model\Data;

use Phalcon\Mvc\Model;

class Request extends Model
{
    protected $id;

    protected $headers = array();

    /**
     * @var string|null
     */
    protected $body;

    public function initialize()
    {
        $this->useDynamicUpdate(true);
        $this->setSource('request_object');
    }

    /**
     * @return mixed
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * @param mixed $id
     * @return Request
     */
    public function setId($id)
    {
        $this->id = $id;
        return $this;
    }

    /**
     * @return array
     */
    public function getHeaders()
    {
        return $this->headers;
    }

    /**
     * @param array $headers
     * @return Request
     */
    public function setHeaders($headers)
    {
        foreach ($headers as $k => $v) {

            if (strlen($v) < 1) {
                unset($headers[$k]);
            }
        }
        $this->headers = $headers;
        return $this;
    }

    /**
     * @return mixed
     */
    public function getBody()
    {
        return $this->body;
    }

    /**
     * @param mixed $body
     * @return Request
     */
    public function setBody($body)
    {
        $this->body = $body;
        return $this;
    }

    public function afterFetch()
    {
        $this->headers = json_decode($this->headers, true);
    }

    public function beforeSave()
    {
        $this->headers = json_encode($this->headers);
    }
}
