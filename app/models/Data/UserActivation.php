<?php

namespace App\Model\Data;

use Httpcb\Mvc\Model\Behavior\RandomId as RandomIdBehavior;
use Phalcon\Forms\Element\Date;
use Phalcon\Mvc\Model\Behavior\SoftDelete;

class UserActivation extends Base
{
    /**
     * @var int
     */
    protected $id;

    /**
     * @var string
     */
    protected $activation_key;

    /**
     * @var int
     */
    protected $user_id;

    /**
     * @var int
     */
    protected $used;

    /**
     * @var string
     */
    protected $password;

    /**
     * @var string
     */
    protected $date;

    /**
     * Initialize method for model.
     */
    public function initialize()
    {
        $this->setSource('user_activation');
        $this->useDynamicUpdate(true);

        // Relationships
        $this->belongsTo('user_id', User::class, 'id', array('alias' => 'User'));

        // Behaviour
        $this->addBehavior(new RandomIdBehavior(array(
            'field' => 'activation_key',
            'length' => 40,
            'expression' => '(used = 0 OR HOUR(TIMEDIFF(date, NOW())) = 0)'
        )));

        $this->addBehavior(new SoftDelete([
            'field' => 'password',
            'value' => null
        ]));

        $this->addBehavior(new SoftDelete([
            'field' => 'used',
            'value' => 1
        ]));
    }

    /**
     * @return int
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * @param int $id
     * @return PasswordLink
     */
    public function setId(int $id): UserActivation
    {
        $this->id = $id;
        return $this;
    }

    /**
     * @return string
     */
    public function getActivationKey()
    {
        return $this->activation_key;
    }

    /**
     * @param string $public_id
     * @return UserActivation
     */
    public function setActivationKey(string $key): UserActivation
    {
        $this->activation_key = $key;
        return $this;
    }

    /**
     * @return int
     */
    public function getUserId()
    {
        return $this->user_id;
    }

    /**
     * @param int $user_id
     * @return UserActivation
     */
    public function setUserId(int $user_id): UserActivation
    {
        $this->user_id = $user_id;
        return $this;
    }

    /**
     * @return string
     */
    public function getPassword()
    {
        return $this->password;
    }

    /**
     * @param string $password
     * @return UserActivation
     */
    public function setPassword(string $password): UserActivation
    {
        $this->password = $password;
        return $this;
    }

    /**
     * Has the link been used?
     *
     * @return bool
     */
    public function isUsed(): bool
    {
        return (bool) $this->used;
    }

    /**
     * Is the link still valid?
     *
     * @return bool
     */
    public function isValid(): bool
    {
        // Used links are not valid.
        if ($this->isUsed()) {
            return false;
        }

        $date = new \DateTime($this->date);
        $now = new \DateTime();

        // Get the differerence in hours.
        $diff = $now->format('U') - $date->format('U');
        $diff = ($diff / 60) / 60;

        // Only valid for an hour.
        return $diff >= 0 && $diff <= 1;
    }

    public function beforeCreate()
    {
        // Creating a new link automatically removes old ones.
        $links = self::find(["user_id = ?0", 'bind' => [$this->user_id]]);

        foreach ($links as $link) {
            $link->delete();
        }
    }
}
