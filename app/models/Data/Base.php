<?php

namespace App\Model\Data;

use Phalcon\Mvc\Model;

class Base extends Model
{
    /**
     * {@inheritdoc}
     *
     * Phalcon throws an exception if no valid snapshot data is present.
     *
     * We override this behaviour by treating no
     * snapshot data (new row) as changed.
     *
     * @param string|array $fieldName
     * @param boolean $allFields
     * @return bool
     */
    public function hasChanged($fieldName = null, bool $allFields = false): bool
    {
        return $this->hasSnapshotData() === false
            || parent::hasChanged($fieldName, $allFields);
    }
}
