<?php

namespace App\Model\Data;

use Httpcb\Mvc\Model\Behavior\RandomId as RandomIdBehavior;
use Phalcon\Mvc\Model;

class Callback extends Model
{

    /**
     *
     * @var integer
     */
    protected $id;

    /**
     *
     * @var integer
     */
    protected $userid;

    /**
     *
     * @var string
     */
    protected $name;

    /**
     *
     * @var string
     */
    protected $public_id;

    /**
     *
     * @var string
     */
    protected $color;

    /**
     *
     * @var string
     */
    protected $created_at;

    /**
     *
     * @var string
     */
    protected $last_request;

    /**
     * Method to set the value of field id
     *
     * @param integer $id
     * @return $this
     */
    public function setId($id)
    {
        $this->id = $id;

        return $this;
    }

    /**
     * Method to set the value of field userid
     *
     * @param integer $userid
     * @return $this
     */
    public function setUserid($userid)
    {
        $this->userid = $userid;

        return $this;
    }

    /**
     * Method to set the value of field name
     *
     * @param string $name
     * @return $this
     */
    public function setName($name)
    {
        $this->name = $name;

        return $this;
    }

    /**
     * Method to set the value of field public_id
     *
     * @param string $public_id
     * @return $this
     */
    public function setPublicId($public_id)
    {
        $this->public_id = $public_id;

        return $this;
    }

    /**
     * Method to set the value of field color
     *
     * @param string $color
     * @return $this
     */
    public function setColor($color)
    {
        $this->color = $color;

        return $this;
    }

    /**
     * Method to set the value of field created_at
     *
     * @param string $created_at
     * @return $this
     */
    public function setCreatedAt($created_at)
    {
        $this->created_at = $created_at;

        return $this;
    }

    /**
     * Method to set the value of field last_request
     *
     * @param string $last_request
     * @return $this
     */
    public function setLastRequest($last_request)
    {
        $this->last_request = $last_request;

        return $this;
    }

    /**
     * Returns the value of field id
     *
     * @return integer
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * Returns the value of field userid
     *
     * @return integer
     */
    public function getUserid()
    {
        return $this->userid;
    }

    /**
     * Returns the value of field name
     *
     * @return string
     */
    public function getName()
    {
        return $this->name;
    }

    /**
     * Returns the value of field public_id
     *
     * @return string
     */
    public function getPublicId()
    {
        return $this->public_id;
    }

    /**
     * Returns the value of field color
     *
     * @return string
     */
    public function getColor()
    {
        return $this->color;
    }

    /**
     * Returns the value of field created_at
     *
     * @return string
     */
    public function getCreatedAt()
    {
        return $this->created_at;
    }

    /**
     * Returns the value of field last_request
     *
     * @return string
     */
    public function getLastRequest()
    {
        return $this->last_request;
    }

    /**
     * Initialize method for model.
     */
    public function initialize()
    {
        // Set table name mapped in the model.
        $this->setSource('callback');

        $this->useDynamicUpdate(true);

        $this->hasMany('id', RequestMeta::class, 'callbackid', array('foreignKey' => true, 'alias' => 'Requests'));
        $this->belongsTo('userid', User::class, 'id', array('foreignKey' => true, 'alias' => 'User'));

        $this->addBehavior(new RandomIdBehavior(array(
            'field' => 'public_id',
            'length' => 8,
        )));
    }

    public function getRequestPaginator($page = 1, $limit = 30)
    {
        return RequestMeta::getPaginator($this, $page, $limit);
    }

    public static function get($pid)
    {
        return parent::findFirst(array(
            'public_id = :pid:',
            'bind'      => array('pid' => $pid),
        ));
    }

    /**
     * @param $userid
     * @param int $page
     * @param int $limit
     * @return \Phalcon\Paginator\AdapterInterface
     */
    public static function getPaginationList($userid, $page = 1, $limit = 30)
    {
        $builder = (new self())->getModelsManager()->createBuilder();

        $builder->from(self::class)
            ->where('userid = :uid:', array('uid' => $userid))
            ->orderBy('created_at DESC');

        $paginator = new \Phalcon\Paginator\Adapter\QueryBuilder(array(
            'builder'   => $builder,
            'page'      => $page,
            'limit'     => $limit
        ));

        return $paginator;
    }
}
