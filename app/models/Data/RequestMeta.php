<?php

namespace App\Model\Data;

use Phalcon\Mvc\Model;
use Localheinz\Json\Printer\Printer as JsonPrinter;

class RequestMeta extends Model
{
    /**
     *
     * @var integer
     */
    protected $id;

    /**
     *
     * @var integer
     */
    protected $callbackid;

    /**
     * Source IP Address
     *
     * @var string
     */
    protected $source_ip;

    /**
     * HTTP Method
     *
     * @var string
     */
    protected $method;

    /**
     * HTTP Request Uri
     *
     * @var string
     */
    protected $uri;

    /**
     * Cached query parameters from request uri.
     *
     * @var array|null
     */
    protected $_uri_query = null;

    /**
     *
     * @var string
     */
    protected $timestamp;

    /**
     * Initialize method for model.
     */
    public function initialize()
    {
        $this->skipAttributes(array('request_object_id'));
        $this->setSource('request_meta');
        $this->useDynamicUpdate(true);

        // Relationships

        $this->belongsTo('callbackid', Callback::class, 'id', array('alias' => 'Callback'));
        $this->hasOne('id', Request::class, 'id', array('alias' => 'RequestObject'));
    }

    /**
     * Method to set the value of field id
     *
     * @param integer $id
     * @return $this
     */
    public function setId($id)
    {
        $this->id = $id;

        return $this;
    }

    /**
     * Method to set the value of field callbackid
     *
     * @param integer $callbackid
     * @return $this
     */
    public function setCallbackid($callbackid)
    {
        $this->callbackid = $callbackid;

        return $this;
    }

    /**
     * @param string $source_ip
     * @return RequestMeta
     */
    public function setSourceIp($source_ip)
    {
        $this->source_ip = (string) $source_ip;
        return $this;
    }

    /**
     * @return mixed
     */
    public function getSourceIp()
    {
        return $this->source_ip;
    }

    /**
     * @param string $method
     * @return RequestMeta
     */
    public function setMethod($method)
    {
        $this->method = (string) $method;
        return $this;
    }

    /**
     * @return string
     */
    public function getMethod()
    {
        return $this->method;
    }

    /**
     * @param string $uri
     * @return RequestMeta
     */
    public function setUri($uri)
    {
        $this->uri = (string) $uri;

        // New uri string, invalidate query.
        $this->_uri_query = null;
        return $this;
    }

    /**
     * @return string
     */
    public function getUri()
    {
        return $this->uri;
    }

    /**
     * @return array
     */
    public function getUriQuery()
    {
        if ($this->_uri_query === null) {

            $query = (string) parse_url($this->getUri(), PHP_URL_QUERY);

            $ret = array();
            foreach (explode('&', $query) as $v) {
                @list($k, $v) = explode('=', $v, 2);
                if (strlen($k) > 0) {
                    $ret[$k] = $v;
                }
            }
            $this->_uri_query = $ret;
        }

        return $this->_uri_query;
    }


    /**
     * Method to set the value of field Timestamp
     *
     * @param string $Timestamp
     * @return $this
     */
    public function setTimestamp($timestamp)
    {
        $this->timestamp = $timestamp;

        return $this;
    }

    /**
     * Returns the value of field id
     *
     * @return integer
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * Returns the value of field callbackid
     *
     * @return integer
     */
    public function getCallbackid()
    {
        return $this->callbackid;
    }

    public function getSize()
    {
        $headers = $this->getHeaders();

        foreach ($headers as $k => $v) {

            if ($k == 'Content-Length') {
                return $v;
            }
        }
        return 0;
    }

    public function getType()
    {
        $headers = $this->getHeaders();

        foreach ($headers as $k => $v) {

            if ($k == 'Content-Type') {
                return substr($v, strrpos($v, '/') + 1);
            }
        }
        return 'Unknown';
    }

    public function getHeaders()
    {
        return $this->getRequestObject()->getHeaders();
    }

    public function getBody()
    {
        $body = $this->getRequestObject()->getBody();

        if ($this->getType() == 'json') {
            $printer = new JsonPrinter();
            return $printer->print($body, '  ');
        }
        return $body;
    }

    /**
     * Returns the value of field Timestamp
     *
     * @return string
     */
    public function getTimestamp()
    {
        return $this->timestamp;
    }

    /**
     * @param Callback $callback
     * @param int $page
     * @param int $limit
     * @return \Phalcon\Paginator\AdapterInterface
     */
    static public function getPaginator(Callback $callback, $page = 1, $limit = 30)
    {
        $builder = (new self())->getModelsManager()->createBuilder();

        $builder->from(self::class)
            ->where('callbackid = :cid:', array('cid' => $callback->getId()))
            ->orderBy('timestamp desc');

        $paginator = new \Phalcon\Paginator\Adapter\QueryBuilder(array(
            'builder'   => $builder,
            'page'      => $page,
            'limit'     => $limit
        ));

        return $paginator;
    }
}
