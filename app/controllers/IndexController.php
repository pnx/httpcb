<?php

namespace App\Controller;

use App\Controller\ControllerBase;

class IndexController extends ControllerBase
{
    public function indexAction()
    {
        $this->view->setMainView('layout-front');
    }

    public function aboutAction()
    {
    }
}
