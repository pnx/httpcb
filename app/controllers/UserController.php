<?php

namespace App\Controller;

use App\Controller\ControllerBase,
    App\Form\UserSettings as UserSettingsForm,
    App\Model\Data\User,
    App\Model\Data\UserActivation,
    App\Model\Data\ActivityLog;

class UserController extends ControllerBase
{
    public function settingsAction()
    {
        $user = $this->_getAuth()->getUser();

        $form = new UserSettingsForm($user);

        if ($this->request->isPost()) {
            $data = $this->request->getPost();

            if ($form->isValid($data)) {

                $new_pw = $form->getValue('passwordNew');
                if (strlen($new_pw) > 0) {

                    $hash = $this->security->hash($new_pw, 12);

                    // User had a password before. just update.
                    if (strlen($user->getPassword()) > 0) {
                        $user->setPassword($hash);
                    }
                    // Else we create a password link and email.
                    else {
                        $activation = new UserActivation();
                        $activation->setUserId($user->getId())
                            ->setPassword($hash)
                            ->save();

                        // Render the email content.
                        $tpl = $this->di->get('template');
                        $content = $tpl->render('mail/password_activation', [
                            'link' => $activation->getActivationKey()
                        ]);

                        // Send the email.
                        $this->di->getMail()->send(
                            'Httpcb password activation',
                            $user->getEmail(),
                            $content
                        );

                        $msg = "For security reasons. Before a password can be created "
                            . "a email has been sent to <strong>{$user->getEmail()}</strong> with "
                            . "a activation link.";

                        $this->flash->notice($msg);
                    }
                }

                $user->save();
                $form->initialize();

                $this->flash->message('success', 'Settings saved!');
            } else {
                $this->flash->message('error', 'Could not save settings');
            }
        }

        $this->view->user = $user;
        $this->view->form = $form;
    }

    public function deleteAction()
    {
        $user = $this->_getAuth()->getUser();

        if ($this->request->isPost()) {
            $data = $this->request->getPost();
        }

        // Delete acc.
        if (isset($data['deleteAcc'])) {

            if (strlen($user->getPassword()) > 0) {
                if (!isset($data['currentpw']) || !$this->security->checkHash($data['currentpw'], $user->getPassword())) {
                    $this->flash->error('The password was not correct. Refusing to delete account.');
                    $this->response->redirect('/settings');
                    return;
                }
            }

            $user->setStatus(User::STATUS_DELETED);
            $user->save();

            // Logout the user.
            $this->auth->clearIdentity();

            $this->flash->success('The account was successfully removed.');
        }

        $this->response->redirect('/settings');
    }

    public function activityAction($page = 1)
    {
        $user = $this->_getAuth()->getUser();

        $paginator = ActivityLog::getPaginationList($user->getId(), $page);

        $this->view->page = $paginator->paginate();
        $this->view->pagination_url = '/user/activity/';
    }

    public function oauthDisconnectAction($provider, $last_unlink_confirmed = false)
    {
        $user = $this->_getAuth()->getUser();

        // Check if we are unlinking the last provider
        if (count($user->getSocialLinks()) <= 1) {

            // If user does not have a password, we wont allow it.
            if (strlen($user->getPassword()) < 1) {
                $msg = 'Unlinking your last OAuth provider cannot be done '
                    . 'if you don\'t have a password as it would be impossible for you to log in.';

                $this->flash->message('error', $msg);
                $this->response->redirect('/settings');
                return;
            }

            // Give a warning to the user about password as the only login option.
            if ($last_unlink_confirmed == false) {

                $url = $this->url->get([
                    'for' => 'oauth-disconnect-confirm',
                    'provider' => $provider,
                    'confirm' => 'confirm',
                ]);

                $msg = '<p>You are about to unlink the last OAuth provider.'
                    . ' Your <strong>only</strong> login option will be <strong>password</strong> if you do this.</p>'
                    . '<p>Are you sure? <a class="alert-link" href="' . $url . '">Yes</a></p>';

                $this->flash->message('warning', $msg);
                $this->response->redirect('/settings');
                return;
            }
        }

        $provider = ucfirst($provider);
        $user->{'set' . $provider . 'Id'}(null);
        $user->save();

        $this->eventsManager->fire('user:onOAuthDisconnect', $user, $provider);

        $this->flash->message('success', "<p><strong>{$provider}</strong> was disconnected</p>");

        $this->response->redirect('/settings');
    }
}
