<?php

namespace App\Controller;

use App\Controller\ControllerBase,
    App\Model\Data\User,
    App\Model\Data\UserActivation,
    App\Form\Login as LoginForm,
    App\Form\Registration as RegistrationForm,
    Httpcb\OAuth\UserData\UserDataInterface,
    Httpcb\Auth\Result;

class AuthController extends ControllerBase
{
    public function indexAction()
    {
        $form = new LoginForm();

        if ($this->request->isPost()) {
            $data = $this->request->getPost();
            if ($form->isValid($data)) {

                $email = $form->getValue('Email');
                $passwd = $form->getValue('Password');

                // Perform login
                $result = $this->auth->login($email, $passwd);
                if ($result->isValid()) {
                    $this->response->redirect('/');
                } else {
                    $this->flash->message('error', "Invalid credentials");
                }
            } else {
                $msg = '<ul>';

                foreach ($form->getMessages() as $message) {
                    $msg .= '<li><strong>' . $message->getField() . '</strong> ' . $message->getMessage() . '</li>';
                }

                $msg .= '</ul>';
                $this->flash->message('error', $msg);
            }
        }

        $this->view->form = $form;
    }

    public function oauthAction($provider_name)
    {
        $client = $this->getDI()->get('oauth', [$provider_name]);

        $code = $this->request->get('code');
        $state = $this->request->get('state');

        // Have code. Authenticate and fetch data.
        if (strlen($code) > 0) {

            try {
                // NOTE: Should pass $state here also.
                $data = $client->authenticate($code);

                // If user is authed already, we connect.
                $user = $this->auth->getUser();
                if ($user) {

                    $name = ucfirst($provider_name);
                    $user->{'set' . $name . 'Id'}($data->getId());
                    $user->save();

                    $this->eventsManager->fire('user:onOAuthConnected', $user, $data);

                    $this->flash->message('success', sprintf("OAuth provider <strong>%s</strong> was connected!", $name));
                    $this->response->redirect('/settings');
                }
                // Perform Auth.
                else {
                    $result = $this->auth->loginOauth($data);

                    if (!$result->isValid()) {

                        if ($result->getCode() == Result::FAILURE_ACCOUNT_SUSPENDED) {
                            $this->flash->message('error', 'Failed to authenticate.');
                            $this->response->redirect('/login');
                            return;
                        }

                        $this->session->set('auth:register:data', $data);
                        $this->response->redirect(['for' => 'user-register']);
                        return;
                    }

                    // User is logged in.
                    $this->response->redirect('/');
                }
            } catch (\Exception $e) {
                throw $e;
                $this->flash->message('error', 'Failed to authenticate.');
                if ($this->auth->getUser()) {
                    $this->response->redirect('/settings');
                } else {
                    $this->response->redirect('/login');
                }
            }
        }
        // No code
        else {
            // redirect to provider and acquire code.
            $this->response->redirect($client->getAuthorizationUrl(), true);
        }
    }

    public function registerAction()
    {
        $data = $this->session->get('auth:register:data');
        if (!($data instanceof UserDataInterface)) {
            $this->response->redirect('/');
            return;
        }

        $user = new User();
        $user->assign(
            $data->toArray(),
            null,
            ['email', 'username', 'firstname', 'lastname']
        );

        $form = new RegistrationForm($user);

        if ($this->request->isPost()) {

            $formData = $this->request->getPost();
            if ($form->isValid($formData)) {

                // Check if email was changed.
                $activationNeeded = false;
                if ($form->getValue('email') !== $data->getEmail()) {
                    $activationNeeded = true;

                    // Set suspended until the email address is confirmed
                    $user->setStatus(User::STATUS_SUSPENDED);
                }

                $user->setOAuthId($data->getProvider(), $data->getId());
                if ($user->save()) {

                    if ($activationNeeded) {

                        $activation = new UserActivation();
                        $activation->setUserId($user->getId())
                            ->save();

                        $content = $this->di->getShared('template')->render('mail/account_activation', [
                            'link' => $activation->getActivationKey()
                        ]);

                        $this->di->getMail()->send('Httpcb account activation', $user->getEmail(), $content);

                        $this->flash->success('User successfully created.');
                        $this->flash->notice("An email has been sent to {$form->getValue('email')} with an activation code.");
                        $this->response->redirect('/login');
                    } else {
                        $this->auth->systemLogin($user);
                        $this->flash->success('User successfully created. Now add your first callback!');
                        $this->response->redirect('/callback/new');
                    }
                } else {
                    $this->flash->error('Could not create user');
                }
            }
            $form->setEntity($formData);
        } else {
            $form->isValid($data->toArray());
        }

        $this->view->provider = $data->getProvider();
        $this->view->form = $form;
    }

    public function logoutAction()
    {
        $this->auth->clearIdentity();
        $this->response->redirect('/');
    }
}
