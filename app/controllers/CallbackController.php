<?php

namespace App\Controller;

use App\Controller\ControllerBase,
    App\Form\CallbackCreate as CreateCallbackForm,
    App\Model\Data\Callback as CallbackModel;

class CallbackController extends ControllerBase
{
    /**
     * @var \Model\Data\User
     */
    protected $_user;

    public function initialize()
    {
        $this->_user = $this->_getAuth()->getUser();
    }

    /**
     * @param $page
     */
    public function listAction($page = 1)
    {
        $paginator = CallbackModel::getPaginationList($this->_user->getId(), $page, 10);

        if ($paginator->paginate()->current > $paginator->paginate()->last) {
            $paginator->setCurrentPage(1);
        }

        $this->view->page = $paginator->paginate();
        $this->view->pagination_url = '/callback/list/';
    }

    /**
     * Create a new test session.
     */
    public function newAction()
    {
        $form = new CreateCallbackForm();

        if ($this->request->isPost()) {
            $data = $this->request->getPost();

            if ($form->isValid($data)) {
                $callback = new CallbackModel();
                $callback->User = $this->_user;

                $callback->setName($form->getValue('Name'));

                $result = $callback->save();
                if ($result) {
                    $callback->refresh();

                    return $this->response->redirect(array(
                        'for' => 'cb-created',
                        'id' => $callback->getPublicId()
                    ));
                } else {
                    foreach ($callback->getMessages() as $msg) {
                        $this->flash->error($msg);
                    }
                }
            } else {
                $msg = '<ul>';

                foreach ($form->getMessages() as $message) {
                    $msg .= '<li><strong>' . $message->getField() . '</strong>: ' . $message->getMessage() . '</li>';
                }

                $msg .= '</ul>';
                $this->flash->message('error', $msg);
            }
        }

        $this->view->form = $form;
    }

    /**
     *
     */
    public function createdAction($id)
    {
        $row = CallbackModel::get($id);
        if (!$row) {
        }
        $this->view->id = $id;
    }

    /**
     * Monitor a test session.
     *
     * @param $id
     * @param $page
     */
    public function showAction($id = null, $page = 1)
    {
        $callback = CallbackModel::findFirst(array(
            'conditions' => 'public_id = ?0 AND userid = ?1',
            'bind' => array($id, $this->_user->getId())
        ));

        if (!$callback) {
            $this->_forward404();
            return;
        }

        $paginator = $callback->getRequestPaginator($page, 30);

        $this->view->item = $callback;
        $this->view->page = $paginator->paginate();
        $this->view->pagination_url = '/callback/show/' . $id . '/';
    }
}
