<?php

namespace App\Controller;

use Phalcon\Mvc\Controller;

/**
 * @property \Httpcb\Auth $auth
 *
 * Class ControllerBase
 * @package App\Controller
 */
class ControllerBase extends Controller
{
    /**
     * Helper method to forward to the 404 - file not found page.
     */
    protected function _forward404()
    {
        $this->dispatcher->forward(array(
            'namespace'     => 'App\\Controller',
            'controller'    => 'error',
            'action'        => 'show404'
        ));
    }

    /**
     * @return App\Auth\Auth
     */
    protected function _getAuth()
    {
        return $this->di->get('auth');
    }
}
