<?php

namespace App\Controller\Backend;

use App\Model\Data\ActivityLog;

class LogController extends \Phalcon\Mvc\Controller
{
    public function onConstruct()
    {
        $this->view->setLayout('side-menu');
    }

    public function indexAction($page = 1)
    {
        $paginator = ActivityLog::getAllPaginationList($page);

        $this->view->page = $paginator->paginate();
        $this->view->pagination_url = '/admin/log/';
    }
}
