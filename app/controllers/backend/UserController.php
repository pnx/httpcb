<?php

namespace App\Controller\Backend;

use App\Model\Data\User,
    App\Form\UserSettings as UserSettingsForm;

class UserController extends \Phalcon\Mvc\Controller
{
    public function onConstruct()
    {
        $this->view->setLayout('side-menu');
    }

    /**
     * @param $page
     */
    public function indexAction($page = 1)
    {
        $paginator = User::getPaginationList($page, 15);

        $this->view->pagination_url = '/admin/user/list/';
        $this->view->page = $paginator->paginate();
    }

    public function newAction()
    {
        $user = new User;
        $form = new UserSettingsForm($user, true);

        if ($this->request->isPost()) {
            $data = $this->request->getPost();
            if ($form->isValid($data)) {

                $new_pw = $form->getValue('passwordNew');
                if (strlen($new_pw) > 0) {
                    $hash = $this->security->hash($new_pw, 12);
                    $user->setPassword($hash);
                }

                $form->initialize();

                $this->flash->message('success', 'User created!');
                $this->response->redirect(['for' => 'backend-user-list']);
                return;
            }

            $this->flash->message('error', 'Could not create user');
        }

        $this->view->user = $user;
        $this->view->form = $form;
        $this->view->pick('user/form');
    }

    public function editAction($id)
    {
        $user = User::findFirstById($id);
        $form = new UserSettingsForm($user, true);

        if ($this->request->isPost()) {
            $data = $this->request->getPost();

            if ($form->isValid($data)) {

                $new_pw = $form->getValue('passwordNew');
                if (strlen($new_pw) > 0) {
                    $hash = $this->security->hash($new_pw, 12);
                    $user->setPassword($hash);
                }
                $user->save();
                $form->initialize();

                $this->flash->message('success', 'User saved!');
            } else {
                $this->flash->message('error', 'Could not save user');
            }
        }

        $this->view->user = $user;
        $this->view->form = $form;
        $this->view->pick('user/form');
    }

    public function statusAction($id, $type)
    {
        $user = User::findFirstById($id);
        $user->setStatus(ucfirst($type));
        $user->save();

        $status = $user->getStatus();
        // Bit of a hack to convert "active" to verb.
        if ($status === User::STATUS_ACTIVE) {
            $status = 'Activated';
        }

        $this->flash->success('The account was: ' . $status);
        $this->response->redirect('/admin');
    }

    public function activationEmailAction($id)
    {
        $user = User::findFirstById($id);
        if ($user) {
            if ($user->isSuspended()) {
                $this->eventsManager->fire('auth:onSentActivation', $user);
                $this->flash->success('Activation email sent to: ' . $user->email);
            } else {
                $this->flash->error('Only suspended users can be sent activation emails.');
            }
        } else {
            $this->flash->error('Invalid user: ' . $id);
        }
        $this->response->redirect('/admin');
    }

    public function impersonateAction($id)
    {
        $user = User::findFirstById($id);

        try {
            $this->auth->impersonate($user);
            $this->response->redirect('/');
        } catch (\Exception $ex) {
            $this->flash->error($ex->getMessage());
            $this->response->redirect('/admin');
        }
    }
}
