<?php

namespace App\Controller;

use App\Controller\ControllerBase,
    App\Model\Data\Callback as CallbackModel,
    App\Model\Data\Request as RequestModel,
    App\Model\Data\RequestMeta as RequestMetaModel,
    App\Model\Data\User,
    App\Model\Data\UserActivation;

class ApiController extends ControllerBase
{
    /**
     * This is the action that the API to be
     * tested should make it's callback to. So we can catch it.
     *
     * @param int $id   The test session id so
     *                  we know what test it belongs to.
     * @return string
     */
    public function endpointAction($id)
    {
        $this->view->disable();

        $allowed_methods = array('GET', 'POST');
        if ($this->request->isMethod($allowed_methods)) {

            $callback = CallbackModel::get($id);

            $request = new RequestModel();

            $request->setHeaders($this->request->getHeaders());
            $request->setBody($this->request->getRawBody());

            $dt = new \DateTime();

            $callback->setLastRequest($dt->format('Y-m-d H:i:s'));

            $meta = new RequestMetaModel();
            $meta->Callback = $callback;
            $meta->RequestObject = $request;

            $meta->setSourceIp($this->request->getClientAddress());
            $meta->setMethod($this->request->isPost() ? 'POST' : 'GET');
            $meta->setUri($this->request->getServer('REQUEST_URI'));

            $result = $meta->save();
            if ($result == false) {
                var_dump($meta->getMessages());
            }
        }
    }

    /**
     * Account/Password activation.
     *
     * @param $id
     */
    public function activationLinkAction($id)
    {
        $link = UserActivation::findFirst(['activation_key = ?0', 'bind' => [$id]]);

        if ($link) {
            if ($link->isValid()) {

                $user = $link->getUser();

                // Save password if any is set.
                if (strlen($link->getPassword()) > 0) {
                    $user->setPassword($link->getPassword());
                    $this->flash->success('Your password has been activated.');
                } else {
                    $user->setStatus(User::STATUS_ACTIVE);
                    $this->flash->success('Your account has been activated.');

                    // Also login the user.
                    $this->auth->systemLogin($user);
                }
                $user->save();
            } else {
                $this->flash->error('This link has expired or has already been used.');
            }

            // Make sure the link is deleted.
            $link->delete();
        } else {
            $this->flash->error('This does not seem to be an active link');
        }
    }
}
