<?php

namespace App\Controller;

use App\Controller\ControllerBase;

class ErrorController extends ControllerBase
{
    public function errorAction()
    {
    }

    public function show404Action()
    {
        // Set status code to 404.
        $this->response->setStatusCode(404);

        // Get the request uri to pass to google :)
        $uri = ltrim($this->request->getURI(), '/');
        $uri = str_replace('/', '+', $uri);
        $this->view->url = $uri;
    }
}
