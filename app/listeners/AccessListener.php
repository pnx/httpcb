<?php

namespace App\Listener;

use Phalcon\Di\Injectable,
    Phalcon\Events\Event,
    Phalcon\Mvc\Dispatcher,
    Phalcon\Mvc\Dispatcher\Exception as DispatcherException;

use Httpcb\Acl;

class AccessListener extends Injectable
{
    protected $_ignored_resources = [
        'index',
        'error'
    ];

    /**
     * @param Event $event
     * @param Dispatcher $dispatcher
     * @return bool
     * @throws DispatcherException
     */
    public function beforeExecuteRoute(Event $event, Dispatcher $dispatcher): bool
    {
        // If we have an identity, fetch type from authed user.
        if ($this->auth->hasIdentity()) {
            $user = $this->auth->getUser();
            $role = $user->getType();
        }
        // Othersize, we default to role.
        else {
            $role = Acl::ROLE_GUEST;
        }

        $resource = $this->_getCurrentResource($dispatcher);

        // Ignore checks for error resource.
        if (in_array($resource, $this->_ignored_resources)) {
            return true;
        }

        // Now, check and redirect user to login page if
        // this role does not have access to this resource.
        if ($this->acl->isAllowed($role, $resource) === false) {

            // Has identity or acl_redirect flag set.
            // Throw a "handler not found" exception in this case.
            if ($this->auth->hasIdentity() || $this->session->has('acl_redirect')) {

                // Unset redirect flag first.
                unset($this->session->acl_redirect);

                $msg = sprintf("Role '%s' not allowed access to resource '%s'", $role, $resource);
                throw new DispatcherException($msg, Dispatcher::EXCEPTION_HANDLER_NOT_FOUND);
            }

            // Redirect to login page
            $this->response->redirect(['for' => 'login']);

            // And set a flag in session. if we do not have access to that
            // resource either. we should not redirect again.
            $this->session->set('acl_redirect', true);


            // Return false to stop the dispatch loop.
            return false;
        }
        return true;
    }

    protected function _getCurrentResource($dispatcher)
    {
        // If default module, only fetch controller name.
        if (strlen($dispatcher->getModuleName()) < 1) {
            return $dispatcher->getControllerName();
        }

        // Otherwise, we follow the syntax "<module>/<controller>"
        return "{$dispatcher->getModuleName()}/{$dispatcher->getControllerName()}";
    }
}
