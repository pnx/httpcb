<?php

namespace App\Listener;

use Phalcon\Di\Injectable,
    Phalcon\Events\Event,
    App\Model\Data\User,
    App\Model\Data\ActivityLog as ActivityLogger,
    Httpcb\OAuth\UserData\UserDataInterface as OAuthUserDataInterface,
    Httpcb\Auth;

class ActivityLog extends Injectable
{
    /**
     * On login event.
     *
     * @param Event $event
     * @param Auth $auth
     * @param $type
     */
    public function onLogin(Event $event, Auth $auth, $type)
    {
        $this->_log($auth->getUser(), sprintf("Logged in (%s)", $type));
    }

    /**
     * On Impersonate event.
     *
     * @param Event $event
     * @param Auth $auth
     * @param User $user The user Impersonating the user in $auth
     */
    public function onImpersonate(Event $event, Auth $auth, User $user)
    {
        $imp = $auth->getUser();
        $this->_log($user, sprintf("Impersonated user (%s:%s)", $imp->getId(), $imp->getUsername()));
    }

    /**
     * @param Event $event
     * @param User $auth
     */
    public function onPasswordCreated(Event $event, User $user)
    {
        $this->_log($user, "Created password");
    }

    /**
     * @param Event $event
     * @param User $auth
     */
    public function onPasswordChanged(Event $event, User $user)
    {
        $this->_log($user, "Changed password");
    }

    /**
     * Fired when a user is connected to a OAuth provider.
     *
     * @param Event $event
     * @param User $user
     * @param OAuthUserDataInterface $provider
     */
    public function onOAuthConnected(Event $event, User $user, OAuthUserDataInterface $provider)
    {
        $this->_log($user, sprintf("OAuth connected (%s)", $provider->getProvider()));
    }

    /**
     * Fired when a user is disconnected from a OAuth provider.
     *
     * @param Event $event
     * @param User $user
     * @param string $providerName
     */
    public function onOAuthDisconnect(Event $event, User $user, $providerName)
    {
        $this->_log($user, sprintf("OAuth disconnected (%s)", $providerName));
    }

    /**
     * Fired when a activation email is sent.
     *
     * @param Event $event
     * @param User $user
     * @param string $providerName
     */
    public function onSentActivation(Event $event, User $user)
    {
        $this->_log($user, sprintf("Activation email sent"));
    }

    protected function _log(User $user, $message)
    {
        $ip = (new \Phalcon\Http\Request())->getClientAddress(true);

        return (new ActivityLogger())->assign([
            'user_id' => $user->getId(),
            'ip' => $ip,
            'message' => $message
        ])->create();
    }
}
