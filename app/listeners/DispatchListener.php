<?php

namespace App\Listener;

use Exception,
    Phalcon\Events\Event,
    Phalcon\Di\Injectable,
    Phalcon\Mvc\Dispatcher,
    Phalcon\Mvc\Dispatcher\Exception as DispatcherException;

/**
 * Class DispatchListener
 *
 * Plugin for forwarding user to 404 (not found) page
 * if a request could not be dispatched.
 */
class DispatchListener extends Injectable
{
    protected $_route_notfound = array(
        'controller'    => 'error',
        'action'        => 'show404'
    );

    protected $_route_error = array(
        'controller'    => 'error',
        'action'        => 'error',
    );

    /**
     * @param Event $event
     * @param Dispatcher $dispatcher
     * @param Exception $exception
     * @return bool
     */
    public function beforeException(Event $event, Dispatcher $dispatcher, Exception $exception)
    {
        // Figure out if this was a exception from dispatcher and that exception
        // was that an controller or action was not found.
        if ($exception instanceof DispatcherException) {
            switch ($exception->getCode()) {
                case Dispatcher::EXCEPTION_HANDLER_NOT_FOUND:
                case Dispatcher::EXCEPTION_ACTION_NOT_FOUND:
                    // in this case, forward to 404 page.
                    $dispatcher->forward($this->_route_notfound);
                    return false;
            }
        }

        $dispatcher->forward($this->_route_error);
        return false;
    }
}
