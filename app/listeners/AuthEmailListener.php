<?php

namespace App\Listener;

use App\Model\Data\User,
    App\Model\Data\UserActivation;

use Phalcon\Di\Injectable,
    Phalcon\Events\Event;

class AuthEmailListener extends Injectable
{
    public function onSentActivation(Event $event, User $user)
    {
        $activation = new UserActivation();
        $activation->setUserId($user->getId())
            ->save();

        $content = $this->di->getShared('template')->render('mail/account_activation', [
            'link' => $activation->getActivationKey()
        ]);

        $this->di->getMail()->send('Httpcb account activation', $user->getEmail(), $content);
    }
}
