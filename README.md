
# HTTP Callback

This repository contains the code that powers [callback.shufflingpixels.com](https://callback.shufflingpixels.com).
An tool that provides logging of HTTP Request.

## Install

This project depends on [composer](https://getcomposer.org/) and [npm](https://www.npmjs.com/).

After those are installed run:

```
$ composer install
$ npm install
$ sh build.sh
```

## Author

Henrik Hautakoski - [henrik.hautakoski@gmail.com](henrik.hautakoski@gmail.com)
